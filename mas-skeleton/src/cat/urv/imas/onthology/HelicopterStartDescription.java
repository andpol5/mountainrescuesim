
package cat.urv.imas.onthology;

import jade.core.AID;
import java.io.Serializable;

/**
 * This class is used to send the starting locations of the helicopters to the coordinator.
 */
public class HelicopterStartDescription implements Serializable{
    
    public int row;
    public int col;
    public AID aid;
    
    public HelicopterStartDescription(int row, int col, AID aid){
        this.row = row;
        this.col = col;
        this.aid = aid;
    }
}
