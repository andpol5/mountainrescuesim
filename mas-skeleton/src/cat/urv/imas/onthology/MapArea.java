
package cat.urv.imas.onthology;

import java.io.Serializable;

public class MapArea implements Serializable{
    private final int minrow;
    private final int maxrow;
    private final int mincol;
    private final int maxcol;
    private final String id;
    private static int idcounter;
    
    public MapArea(int minrow, int maxrow, int mincol, int maxcol){
        this.minrow = minrow;
        this.maxcol = maxcol;
        this.mincol = mincol;
        this.maxrow = maxrow;
        id = Integer.toString(idcounter);
        idcounter++;
    }

    public MapArea() {
        this.minrow = 0;
        this.maxcol = 0;
        this.mincol = 0;
        this.maxrow = 0;
        id = Integer.toString(-1);
    }
    
    public boolean inArea(int row, int col){
        if (row < minrow) return false;
        if (row > maxrow) return false;
        if (col < mincol) return false;
        if (col > maxcol) return false;
        return true;
    }

    /**
     * @return the minrow
     */
    public int getMinrow() {
        return minrow;
    }

    /**
     * @return the maxrow
     */
    public int getMaxrow() {
        return maxrow;
    }

    /**
     * @return the mincol
     */
    public int getMincol() {
        return mincol;
    }

    /**
     * @return the maxcol
     */
    public int getMaxcol() {
        return maxcol;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
}
