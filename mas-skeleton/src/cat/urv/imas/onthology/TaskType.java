
package cat.urv.imas.onthology;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Andrei Mihai
 */
public enum TaskType {
    NONE,
    SAVE_X,
    X_TO_SAFETY,
    GO_IDLE,
    X_TO_HOSPITAL

}
