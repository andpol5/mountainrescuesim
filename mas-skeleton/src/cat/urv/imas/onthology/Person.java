
package cat.urv.imas.onthology;


public class Person implements java.io.Serializable {
    final int row;
    final int col;
    final boolean light;
    final int stepToDie;
    
    public Person(int row, int col, boolean light, int stepToDie){
        this.row = row;
        this.col = col;
        this.light = light;
        this.stepToDie = stepToDie;
    }

    public Person() {
        this.row = 0;
        this.col = 0;
        this.light = true;
        this.stepToDie = Integer.MAX_VALUE;
    }
    
    public int getStepToDie(){
        return stepToDie;
    }
    public int getRow(){
        return row;
    }
    public int getCol(){
        return col;
    }
    public boolean getLight(){
        return light;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Person)){
            return false;
        }
        Person o = (Person)obj;
        if(o.row != this.row){
            return false;
        }
        if(o.col != this.col){
            return false;
        }
        if(o.light != this.light){
            return false;
        }
        if(o.stepToDie != this.stepToDie){
            return false;
        }
        return true;
    }
}
