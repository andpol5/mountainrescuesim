package cat.urv.imas.onthology;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.CellType;
import cat.urv.imas.map.PathCell;
import jade.core.AID;

import java.util.*;

public class GameMap implements java.io.Serializable {

    /**
     * City map.
     */
    private Cell[][] map;
    
     /**
     * Step counter is used to control when injured people must die, when avalanches must end
     * or when the game ends.
     */
    int step = 0;

    /**
     * Computed summary of the position of agents in the city. For each given
     * type of mobile agent, we get the list of their positions.
     */
    private Map<AgentType, List<Cell>> agentList;


    public Cell[][] getMap() {
        return map;
    }

    public void setMap(Cell[][] map) {
        this.map = map;
    }
        
    /**
     *  Increases the step counter.
     */
    public void nextStep(){
        step++;
    }
    
    /**
     * Returns the current step.
     */
    public int getStep(){
        return step;
    }


    public Map<AgentType, List<Cell>> getAgentList() {
        return agentList;
    }

    public void setAgentList(Map agentList) {
        this.agentList = agentList;
    }
    
    public List<Cell> getCells(CellType type){
        List<Cell> cells = new ArrayList<Cell>();
        for (Cell[] row: map){
            for(Cell c : row){
                if (c.getCellType().equals(type))
                    cells.add(c);
            }
        }
        return cells;
    }
    
    public List<Cell> getCells(){
        List<Cell> cells = new ArrayList<>();
        for (Cell[] row: map){
            cells.addAll(Arrays.asList(row));
        }
        return cells;
    }
    
    public Cell getNearestCell(CellType type,Integer[] location){        
        
        List<Cell> cellsOfType = getCells(type);
        assert cellsOfType.size() > 0: "There is no cell with the type " + type.toString();
        
        Cell closestCell = cellsOfType.get(0);
        int minDistance = Math.abs(closestCell.getRow() - location[0]) + Math.abs(closestCell.getCol() - location[1]);
        for (Cell c: cellsOfType){
            int distance = Math.abs(c.getRow() - location[0]) + Math.abs(c.getCol() - location[1]);
            if (distance < minDistance){
                minDistance = distance;
                closestCell = c;
            }
        }
        return closestCell;
    }
    
    
    public Cell getNearestCell(CellType type, Integer[] location, MapArea area){        
        List<Cell> cellsOfType = getCells(type);
        
        List<Cell> cellsOfTypeinArea = new ArrayList<>();
        for (Cell c: cellsOfType){
            if(area.inArea(c.getRow(), c.getCol())){
                cellsOfTypeinArea.add(c);
            }
        }
        assert cellsOfTypeinArea.size() > 0: "There is no cell with the type " + type.toString() + " in the area.";
        
        Cell closestCell = cellsOfTypeinArea.get(0);
        int minDistance = Math.abs(closestCell.getRow() - location[0]) + Math.abs(closestCell.getCol() - location[1]);
        for (Cell c: cellsOfTypeinArea){
            int distance = Math.abs(c.getRow() - location[0]) + Math.abs(c.getCol() - location[1]);
            if (distance < minDistance){
                minDistance = distance;
                closestCell = c;
            }
        }
        return closestCell;
    }
    
    public int howManyAgents(AgentType type){
        synchronized(this){
            List<Cell> cellsWithType = agentList.get(type);
            int agentsCount = 0;

            for(int i=0; i<cellsWithType.size(); i++){
                try{
                    agentsCount += cellsWithType.get(i).howManyAgents(type);
                }catch (NullPointerException e){}
            }
            return agentsCount;
        }
    }

    /**
     * Add agent to map and to the cell
     *
     * @param type agent type.
     * @param cell cell where appears the agent.
     */
    public void addAgentToMap(AgentType type, Cell cell, AID aid) throws Exception {
        List<Cell> list = agentList.get(type);

        if (list == null) {
            list = new ArrayList();
            this.agentList.put(type, list);
            list.add(cell);
        } else {
            if (!list.contains(cell)) {
                list.add(cell);
            }
        }
        cell.addAgent((aid == null) ? new InfoAgent(type) : new InfoAgent(type, aid));
    }

    /**
     * Removes an agent from map and from agent list
     *
     * @param type
     * @param cell
     * @param aid
     */
    public void removeAgent(AgentType type, Cell cell, AID aid) throws Exception {
        cell.removeAgent(new InfoAgent(type, aid));

        if(cell.howManyAgents(type)==0){
            List<Cell> cells = this.agentList.get(type);
            if(cells != null){
                cells.remove(cell);
            }
        }
    }
    
     /**
     * Returns a list a cells that represents the path found by an a star algorithm.
     */
    public Queue<Cell> helicopterPathFinding(Integer[] startLocation, Integer[] finalLocation,
                                        List<CellType> blockingCellTypes){
        Queue<Cell> path = new LinkedList<>();
        
        // not very optimal with big maps, but again it is no really a big problem 
        // for our project
        Integer[][] scoreMap = new Integer[map.length][map[0].length];
        for (int i = 0; i < map.length; i++)
            for (int j = 0; j < map[0].length; j++)
                scoreMap[i][j] = Integer.MAX_VALUE;
        
        // Using a star to create the score map
        PriorityQueue<Integer[]> toSearch = new PriorityQueue<>(10, new Comparator<Integer[]>(){
            @Override
            public int compare(Integer[] o1, Integer[] o2) {
                return Integer.compare(o1[2], o2[2]);
            }
            
        });
        
        toSearch.add(new Integer[] {startLocation[0],startLocation[1],1});
        while (true){
            Integer[] nextLocation = toSearch.poll();
            if (nextLocation == null) return path;
            if ((nextLocation[0] == finalLocation[0]) && (nextLocation[1] == finalLocation[1])){
                break;
            }
            
            List<Cell> surroundingCells = retrieveSurroundingCells(map[nextLocation[0]][nextLocation[1]]);
            for (Cell c: surroundingCells){
                boolean pathBlocked = false;
                for (CellType type: blockingCellTypes){
                    if (c.getCellType() == type){
                        if (type == CellType.PATH){
                            PathCell cell = (PathCell)c;
                            if (cell.isAvalanche()){
                                pathBlocked = true;
                            }
                        } else{
                            pathBlocked = true;
                        }
                    }
                }
                if (!pathBlocked){
                    int crow = c.getRow();
                    int ccol = c.getCol();
                    int srow = startLocation[0];
                    int scol = startLocation[1];
                    boolean row = (crow == srow);
                    boolean col = (scol == ccol);
                    if (!(row && col)){
                        int score = aStarScore(c, startLocation, finalLocation);
                        scoreMap[c.getRow()][c.getCol()] = score;
                        toSearch.add(new Integer[] {c.getRow(), c.getCol(), score});
                    }
                }
            }
        }
        
        //now create the path using the score map
        Integer[] nextLoc = startLocation.clone();
        List<Cell> alreadyAddedToPath = new ArrayList<>();
        while (true){
            if ((nextLoc[0] == finalLocation[0]) && (nextLoc[1] == finalLocation[1])){
                break;
            }
            List<Cell> surroundingCells = retrieveSurroundingCells(map[nextLoc[0]][nextLoc[1]]);
            int minscore = Integer.MAX_VALUE;
            for (Cell c:surroundingCells){
                if (!alreadyAddedToPath.contains(c)){
                    if (minscore > scoreMap[c.getRow()][c.getCol()]){
                        minscore = scoreMap[c.getRow()][c.getCol()];
                        nextLoc[0] = c.getRow();
                        nextLoc[1] = c.getCol();
                    }
                }
            }
            
            alreadyAddedToPath.add(map[nextLoc[0]][nextLoc[1]]);
            path.add(map[nextLoc[0]][nextLoc[1]]);
        }
       
        return path;
    }
    
     /**
     * Returns a list a cells that represents the path found by an a star algorithm.
     * It may be slow to run a* every step for every rural-injured person pair, but
     * for our little project is not really a problem.
     */
    public Queue<Cell> ruralPathFinding(Integer[] startLocation, Integer[] finalLocation,
                                        List<CellType> blockingCellTypes, int maxlength){
        Queue<Cell> path = new LinkedList<>();
        
        // not very optimal with big maps, but again it is no really a big problem 
        // for our project
        Integer[][] scoreMap = new Integer[map.length][map[0].length];
        for (int i = 0; i < map.length; i++)
            for (int j = 0; j < map[0].length; j++)
                scoreMap[i][j] = Integer.MAX_VALUE;
        
        // Using a star to create the score map
        PriorityQueue<Integer[]> toSearch = new PriorityQueue<>(10, new Comparator<Integer[]>(){
            @Override
            public int compare(Integer[] o1, Integer[] o2) {
                return Integer.compare(o1[2], o2[2]);
            }
            
        });
        
        toSearch.add(new Integer[] {startLocation[0],startLocation[1],1});
        List<Cell> alreadyAddedToSearch = new ArrayList<>();
        while (true){
            Integer[] nextLocation = toSearch.poll();
            if (nextLocation == null) return path;
            if ((nextLocation[0].equals(finalLocation[0])) && (nextLocation[1].equals(finalLocation[1]))){
                break;
            }
            
            List<Cell> surroundingCells = retrieveSurroundingCells(map[nextLocation[0]][nextLocation[1]]);
            for (Cell c: surroundingCells){
                
                if (!pathBlocked(c, blockingCellTypes)){
                    int crow = c.getRow();
                    int ccol = c.getCol();
                    int srow = startLocation[0];
                    int scol = startLocation[1];
                    boolean row = (crow == srow);
                    boolean col = (scol == ccol);
                    if (!(row && col)){
                        if (!alreadyAddedToSearch.contains(c)){
                            int score = aStarScore(c, startLocation, finalLocation);
                            scoreMap[c.getRow()][c.getCol()] = score;
                            toSearch.add(new Integer[] {c.getRow(), c.getCol(), score});
                            alreadyAddedToSearch.add(c);
                        }
                    }
                }
            }
        }
        
        //now create the path using the score map
        Integer[] nextLoc = finalLocation.clone();
        List<Cell> alreadyAddedToPath = new ArrayList<>();
        ArrayList<Cell> listPath = new ArrayList<>();
        listPath.add(map[finalLocation[0]][finalLocation[1]]);
        Cell startlocationCell = map[startLocation[0]][startLocation[1]];
        while (true){
            Cell nextCell = map[nextLoc[0]][nextLoc[1]];
            if (startlocationCell.isAdjacent(nextCell)){
                break;
            }
            List<Cell> surroundingCells = retrieveSurroundingCells(map[nextLoc[0]][nextLoc[1]]);
            int maxscore = Integer.MIN_VALUE;
            Cell maxcell = null;
            for (Cell c:surroundingCells){
                
                if (!pathBlocked(c, blockingCellTypes) && scoreMap[c.getRow()][c.getCol()] < Integer.MAX_VALUE){
                    if (scoreMap[c.getRow()][c.getCol()] > maxscore){
                        if (!alreadyAddedToPath.contains(c)){
                            maxscore = scoreMap[c.getRow()][c.getCol()];
                            maxcell = c;
                            nextLoc[0] = c.getRow();
                            nextLoc[1] = c.getCol();
                        }
                    }
                }
            }
            if (!alreadyAddedToPath.contains(maxcell)){
                listPath.add(map[nextLoc[0]][nextLoc[1]]);
                alreadyAddedToPath.add(map[nextLoc[0]][nextLoc[1]]);
                if (alreadyAddedToPath.size() > maxlength){
                    path.clear();
                    return path;
                }
            }
            
        }
        Collections.reverse(listPath);
        for (Cell c: listPath){
            path.add(c);
        }
        return path;
    }
    
    /*
    It is the sum of manhattan distances from start tu the cell, and from the cell to finish,
    with the distance from start to cell being reduced since it is more important to be closer to the
    final cell
    */
    private int aStarScore(Cell c, Integer[] startLocation, Integer[] finalLocation){
        
        int distanceToStart = Math.abs(startLocation[0] - c.getRow()) + Math.abs(startLocation[1] - c.getCol());
        int distanceToEnd = Math.abs(finalLocation[0] - c.getRow()) + Math.abs(finalLocation[1] - c.getCol());

        return distanceToEnd + distanceToStart/10;
    }
    
    /**
     * Retrieves the surrounding cells of a cell
     */
    public List<Cell> retrieveSurroundingCells(Cell cell) {
        
        int row = cell.getRow();
        int col = cell.getCol();

        List<Cell> surroundingCells = new LinkedList<>();
        for(int i = -1; i<2; i++ ){
            if (row+i >= 0 && row + i < map.length){
                for(int j = -1; j<2;j++){
                    if ((col+j >= 0) && (col + j < map[0].length) && !(j==0 && i == 0)){
                        surroundingCells.add(map[row+i][col+j]);
                    }
                }
            }

        }

        return surroundingCells;
    }

    private boolean pathBlocked(Cell c, List<CellType> blockingCellTypes) {
        boolean blocked = false;
        for (CellType type: blockingCellTypes){
            if (c.getCellType() == type){
                if (type == CellType.PATH){
                    PathCell cell = (PathCell)c;
                    if (cell.isAvalanche()){
                        blocked = true;
                    }
                } else{
                    blocked = true;
                }
            }
        }
        return blocked;
    }
}
