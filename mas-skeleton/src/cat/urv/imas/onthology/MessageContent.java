/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.onthology;

/**
 * Content messages for inter-agent communication.
 */
public class MessageContent {
    
    /**
     * Message sent from Coordinator agent to System agent to get the whole
     * city information.
     */
    public static final int GET_MAP = 0;
    
    /**
     * Message from the Rural/Helicopter agents to the System agent to indicate 
     * movement.
     *
     */
    public static final int FINISH_MOVEMENT = 1;


    /**
     * Message from the System agent the Rural/Helicopter agents to make
     * movement.
     *
     */
    public static final int START_STEP = 2;

    /**
     * Message from the System agent to the Rural and Helicopter agents to tell 
     * them of the available cells for movement.
     *
     */
    public static final int SURROUNDING_CELLS = 3;
    
    
        /**
     * Message from lower level agents to higher level agents to indicate they finished
     * actions for that step.
     *
     */
    public static final int READY = 4;
    
    public static final int CN = 5;
        
    public static final int VOTE = -6;
    
    public static final String CONTRACT_NET = "contract net";
    
    public static final String DO_ACTION = "do action";
    
    public static final String CFP = "cfp";
    
    public static final String END_CN = "end contract net";
    
    public static final String PICK_UP = "picked up a person";
    
    public static final String SAVED = " saved a person";
    
    public static final String VOTING = " start voting";

}
