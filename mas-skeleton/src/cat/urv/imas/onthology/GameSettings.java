/**
 * IMAS base code for the practical work. Copyright (C) 2014 DEIM - URV
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.onthology;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.InjuredPeople;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Current game settings. Cell coordinates are zero based: row and column values
 * goes from [0..n-1], both included.
 *
 * Use the GenerateGameSettings to build the game.settings configuration file.
 *
 */
@XmlRootElement(name = "GameSettings")
public class GameSettings implements java.io.Serializable {

    /* Default values set to all attributes, just in case. */
    /**
     * Seed for random numbers.
     */
    private float seed = 0.0f;
    /**
     * Capacity of helicopter, in number of people.
     */
    private int peoplePerHelicopter = 2;
    /**
     * Number of people loaded into a helicopter per simulation step.
     */
    private int loadingSpeed = 1;
    /**
     * Chance to appear avalanche every step
     */
    private int avalancheChance = 5;
    /**
     * Chance for injured people to appear every step
     */
    private int injuredChance = 5;
    /**
     * Maximum number of people that can appear each step
     */
    private int maximumPeoplePerStep = 3;
    /**
     * Maximum number of people that can appear each step
     */
    private int maximumAvalanchesPerStep = 3;
    /**
     * Number of steps an avalanche can take long.
     */
    private int avalancheDuration = 5;
    /**
     * After this number of steps, injured people will die.
     */
    private int stepsToFreeze = 20;
    /**
     * This sets the light severity ratio of injured people. Grave severity will
     * be 100 - lightSeverity.
     */
    private int lightSeverity = 90;
    /**
     * Cost (think about money) for each person rescued by a rural agent.
     */
    private int ruralAgentCost = 1;
    /**
     * Cost (think about money) for each person rescued by a helicopter.
     */
    private int helicopterCost = 20;
    /**
     * Total number of simulation steps.
     */
    private int simulationSteps = 100;

     /**
     * Contains the current injured persons in the game
     */
    private List<Person> currentInjured = new ArrayList<>();

    protected GameMap gameMap = new GameMap();

    /**
     * Computed summary of the available list of injured people.
     */
    protected List<InjuredPeople> injuredPeople;
    /**
     * Title to set to the GUI.
     */
    protected String title = "Demo title";



    public GameMap getGameMap() {
        return gameMap;
    }

    public float getSeed() {
        return seed;
    }

    @XmlElement(required = true)
    public void setSeed(float seed) {
        this.seed = seed;
    }

    public int getSimulationSteps() {
        return simulationSteps;
    }
    
    public List<Person> getCurrentInjured(){
        return currentInjured;
    }

    @XmlElement(required = true)
    public void setSimulationSteps(int simulationSteps) {
        this.simulationSteps = simulationSteps;
    }

    public String getTitle() {
        return title;
    }

    @XmlElement(required = true)
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets the full current city map.
     *
     * @return the current city map.
     */
    @XmlTransient
    public Cell[][] getMap() {
        return gameMap.getMap();
    }

    /**
     * Gets the cell given its coordinate.
     *
     * @param row row number (zero based)
     * @param col column number (zero based).
     * @return a city's Cell.
     */
    public Cell get(int row, int col) {
        return gameMap.getMap()[row][col];
    }

    public int getPeoplePerHelicopter() {
        return peoplePerHelicopter;
    }

    @XmlElement(required = true)
    public void setPeoplePerHelicopter(int peoplePerHelicopter) {
        this.peoplePerHelicopter = peoplePerHelicopter;
    }

    public int getLoadingSpeed() {
        return loadingSpeed;
    }

    @XmlElement(required = true)
    public void setLoadingSpeed(int loadingSpeed) {
        this.loadingSpeed = loadingSpeed;
    }

    public int getAvalancheDuration() {
        return avalancheDuration;
    }

    @XmlElement(required = true)
    public void setAvalancheDuration(int avalancheDuration) {
        this.avalancheDuration = avalancheDuration;
    }

    public int getStepsToFreeze() {
        return stepsToFreeze;
    }

    @XmlElement(required = true)
    public void setStepsToFreeze(int stepsToFreeze) {
        this.stepsToFreeze = stepsToFreeze;
    }

    public int getLightSeverity() {
        return lightSeverity;
    }

    @XmlElement(required = true)
    public void setLightSeverity(int lightSeverity) {
        this.lightSeverity = lightSeverity;
    }

    public int getRuralAgentCost() {
        return ruralAgentCost;
    }

    @XmlElement(required = true)
    public void setRuralAgentCost(int ruralAgentCost) {
        this.ruralAgentCost = ruralAgentCost;
    }

    public int getHelicopterCost() {
        return helicopterCost;
    }

    @XmlElement(required = true)
    public void setHelicopterCost(int helicopterCost) {
        this.helicopterCost = helicopterCost;
    }

    @XmlTransient
    public Map<AgentType, List<Cell>> getAgentList() {
        return gameMap.getAgentList();
    }

    @XmlTransient
    public List<InjuredPeople> getInjuredPeople() {
        return injuredPeople;
    }

    public String toString() {
        return "Game settings";
    }

    public String getShortString() {
        return "Game settings: agent related string";
    }
    
    public int getAvalancheChance(){
        return avalancheChance;
    }
    
    public int getInjuredChance(){
        return injuredChance;
    }
    
    public int getMaximumPeoplePerStep(){
        return maximumPeoplePerStep;
    }
    
    public int getMaximumAvalanchesPerStep(){
        return maximumAvalanchesPerStep;
    }

    /**
     * Removes dead people from the current injured list;
     */
    public void cleanDeadpeople() {
        List<Person> toRemove = new ArrayList<Person>();
        for (Person p:currentInjured){
            if (p.getStepToDie() <= gameMap.getStep()){
                toRemove.add(p);
            }
        }
        for (Person p:toRemove){
            currentInjured.remove(p);
        }
    }

    public void removeInjuredPerson(Person person){
        Person toRemove = null;
        for (Person p : currentInjured) {
            if (p.getCol() == person.getCol() && p.getRow() == person.getRow() && p.getStepToDie() == person.getStepToDie()) {
                toRemove = p;
                break;
            }
        }
        if(toRemove != null) {
            currentInjured.remove(toRemove);
        }

    }

}
