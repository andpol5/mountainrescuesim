
package cat.urv.imas.onthology;


public class Task {
    
    TaskType type;
    int row;
    int col;
    private Person saved;
    
    
    public Task(){
        type = TaskType.NONE;
        row = -1;
        col = -1;
    };
    
    public void changeType(TaskType newType, int newRow, int newCol){
        type = newType;
        col = newCol;
        row = newRow;
        saved = null;
    }
    
    
    
    public void changeType(TaskType newType){
        type = newType;
        col = -1;
        row = -1;
        saved = null;
    }
    
    public void changeType(TaskType newType, Person injured){
        type = newType;
        col = -1;
        row = -1;
        saved = injured;
    }
    
    public int getRow(){
        return row;
    }
    
    public void setRow(int newRow){
        row = newRow;
    }
    
    public int getCol(){
        return col;
    }
    
    public void setCol(int newCol){
        col = newCol;
    }
    
    public TaskType getType(){
        return type;
    }

    /**
     * @return the saved
     */
    public Person getSaved() {
        return saved;
    }

    public void changeType(TaskType newType, Person saved, int newRow, int newCol) {
        type = newType;
        col = newCol;
        row = newRow;
        this.saved = saved
;    }
}
