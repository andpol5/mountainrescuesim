
package cat.urv.imas.behaviour.rural;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.RuralAgent;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.CellType;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import cat.urv.imas.onthology.TaskType;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import cat.urv.imas.onthology.Pair;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Queue;


public class RuralVotingBehaviour extends OneShotBehaviour {

    RuralAgent rural;
    
    @Override
    public void action() {
        rural = (RuralAgent)myAgent;
        
        // First clear the save x task
        if (rural.getTask().getType() == TaskType.SAVE_X){
            rural.getTask().changeType(TaskType.NONE);
        }
        
        // Wait for list of injured persons form rural
        MessageTemplate mt = MessageTemplate.and(
                MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                MessageTemplate.MatchProtocol(MessageContent.VOTING)
                );
        ACLMessage reply = rural.blockingReceive(mt);
        
        // if the rural is busy the send back an null then add a make move behaviour
        if (rural.isBusy()){
            sendBack(null);
            rural.addBehaviour(new RuralMakeActionBehaviour());
            rural.getPath().clear();
            return;
        }
        // Otherwise continue with the voting
        
        // get the list of injured people from the message
        List<Person> injuredPeople = new ArrayList<>();
        try{
            injuredPeople = (List < Person >)reply.getContentObject();
        }catch (UnreadableException e){rural.log(e.toString()); assert false;}
        
        // vote
        List< Pair<Person, Integer>>  vote = makeVote(injuredPeople);
        
        // send the vote back to the rural coordinator
        sendBack(vote);
        
        // wait for results
        waitOrder();
        
        // add a make action behaviour
        rural.addBehaviour(new RuralMakeActionBehaviour());
    }

    private void sendBack(List<Pair<Person, Integer>> vote) {
            // Search rural coordinator
            ServiceDescription searchCriterionRuralCoordinator = new ServiceDescription();
            searchCriterionRuralCoordinator.setType(AgentType.RURAL_AGENT_COORDINATOR.toString());
            AID ruralCoord = UtilsAgents.searchAgent(myAgent, searchCriterionRuralCoordinator);
            
            ACLMessage message = new ACLMessage(MessageContent.VOTE);
            message.setProtocol(MessageContent.VOTING);
            try{
                message.setContentObject((Serializable)vote);
            } catch (IOException e){rural.log(e.toString()); assert false;}
            
            message.clearAllReceiver();
            message.addReceiver(ruralCoord);
            rural.send(message);
    }

    private void waitOrder() {
        MessageTemplate mt = MessageTemplate.and(
                MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
                MessageTemplate.MatchProtocol(MessageContent.VOTING)
                );
        ACLMessage reply = rural.blockingReceive(mt);
        
        Person p = null;
        try{
            p = (Person)reply.getContentObject();
        }catch (UnreadableException e){rural.log(e.toString()); assert false;}
        
        if (p == null){
            // then there is no person to save for this rural so we set a go idle task
            rural.getTask().changeType(TaskType.NONE);
        } // otherwise we set a save x task
        else{
            rural.getTask().changeType(TaskType.SAVE_X, p, p.getRow(), p.getCol());
            rural.getPath().clear();
        }
    }

    private List< Pair<Person, Integer>> makeVote(List<Person> injuredPeople) {
        List<Pair<Person, Integer>> vote = new ArrayList<>();
        
        for (Person p: injuredPeople){
            List<CellType> blockers = new ArrayList<>();
            blockers.add(CellType.MOUNTAIN);
            blockers.add(CellType.HOSPITAL);
            blockers.add(CellType.PATH);            
            Queue<Cell> path = rural.getGameMap().ruralPathFinding(
                    new Integer[]{rural.getMyCell().getRow(),rural.getMyCell().getCol()},
                    new Integer[]{p.getRow(), p.getCol()},
                    blockers, 20);

            
            int steps = path.size();
            if (steps < 1){
                // then it is either there or there is no path
                if ((rural.getMyCell().getCol() != p.getCol()) && (rural.getMyCell().getRow()!= p.getRow())){
                    // then there is no path
                    vote.add(new Pair<Person, Integer>(p, Integer.MAX_VALUE));
                } else {
                    vote.add(new Pair<Person, Integer>(p, steps));
                }
            } else{
                vote.add(new Pair<Person, Integer>(p, steps));
            }
        }
        
        // Now sort the votes depending on the steps to live and number of steps
        // That is - actually vote
        Collections.sort(vote, new Comparator<Pair<Person, Integer>>(){
            @Override
            public int compare(Pair<Person, Integer> o1, Pair<Person, Integer> o2) {
                int o1steps = o1.getValue();
                int o2steps = o2.getValue();
                int o1stepsTolive = o1.getKey().getStepToDie() - rural.getGameMap().getStep();
                int o2stepsTolive = o2.getKey().getStepToDie() - rural.getGameMap().getStep();
                
                // it is important to save people with few steps to die first
                int scoreo1 = Integer.MAX_VALUE;
                if (o1steps < Integer.MAX_VALUE){
                    scoreo1 = o1steps + o1stepsTolive;
                }
                
                int scoreo2 = Integer.MAX_VALUE;
                if (o2steps < Integer.MAX_VALUE){
                    scoreo2 = o2steps + o2stepsTolive;
                }
                
                
                return Integer.compare(scoreo1, scoreo2);
            }
            
        });
        
        return vote;
    }
    
}
