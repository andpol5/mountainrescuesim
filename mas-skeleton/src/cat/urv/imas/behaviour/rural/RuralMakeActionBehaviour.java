
package cat.urv.imas.behaviour.rural;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.RuralAgent;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.CellType;
import cat.urv.imas.map.PathCell;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import cat.urv.imas.onthology.TaskType;
import jade.core.behaviours.OneShotBehaviour;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class RuralMakeActionBehaviour extends OneShotBehaviour{
    
    RuralAgent agent;

    @Override
    public void action() {
        
        agent = (RuralAgent)myAgent;
        
        switch(agent.getTask().getType()){
        case GO_IDLE:
            goIdle(new Integer[]{agent.getTask().getRow(),agent.getTask().getCol()});
            break;
        case SAVE_X:
            saveX(new Integer[]{agent.getTask().getRow(),agent.getTask().getCol()});
            break;
        case X_TO_SAFETY:
            xToMountainHut(new Integer[]{agent.getTask().getRow(),agent.getTask().getCol()}, agent.getTask().getSaved());
            break;
        case NONE:
            goIdle(new Integer[]{agent.getTask().getRow(),agent.getTask().getCol()});;
        }
        
        if (agent.getPickedUp().size() > 0){
            UtilsAgents.sendReady(agent, AgentType.RURAL_AGENT_COORDINATOR, (Serializable)agent.getPickedUp(),
                                    MessageContent.PICK_UP);
        } else {
            UtilsAgents.sendReady(agent, AgentType.RURAL_AGENT_COORDINATOR, (Serializable)agent.getSaved(),
                                    MessageContent.SAVED);
        }
        agent.getSaved().clear();
        agent.getPickedUp().clear();
        agent.addBehaviour(new RuralStartStepBehaviour());
        
    }

    private void goIdle(Integer[] location) {
        // if it is already on an mountain hut then it has nothing to do this turn
        if (agent.getMyCell().getCellType() == CellType.MOUNTAIN_HUT){
            // Make sure that the path is empty
            agent.getPath().clear();
            return;
        }
        
        // if the location of the mountin hut is unknown then search for the nearest 
        // mountin hut in the area
        if (location[0] == -1 ){
            Cell mountainHut = agent.getGameMap().getNearestCell(
                                    CellType.MOUNTAIN_HUT,
                                    new Integer[]{agent.getMyCell().getRow(),agent.getMyCell().getCol()}
                                    );
            location[0] = mountainHut.getRow();
            location[1] = mountainHut.getCol();
            List<CellType> blockers = new ArrayList<>();
            blockers.add(CellType.MOUNTAIN);
            blockers.add(CellType.HOSPITAL);
            blockers.add(CellType.PATH);
            agent.setPath(agent.getGameMap().ruralPathFinding(
                            new Integer[]{agent.getMyCell().getRow(),agent.getMyCell().getCol()},
                            location ,
                            blockers, 100));
        }
        
        
        // if there is no path to the mountain hut, then do nothing
        if (agent.getPath().size() < 1) return;
        
        Cell nextCell = agent.getPath().poll();
        
        try{
            if (nextCell != null )
                agent.moveToOtherCell(nextCell);
        } catch (Exception e) {
            e.printStackTrace(); assert false;
        }
    }

    private void saveX(Integer[] location) {
        assert location[0] > -1 && location[1] > -1: "Unknown location of the person to be saved!";
        
        // If we are on the cell with the person to be saved
        boolean sameCell = (location[0] == agent.getMyCell().getRow()) && 
                           (location[1] == agent.getMyCell().getCol());
        
        if (sameCell){
            
            // ---------------  If Task Finished ----------------------
            // clear the path since we will not use it anymore
            agent.getPath().clear();
            
            PathCell cell = (PathCell)agent.getGameMap().getMap()[location[0]][location[1]];
            Person saved = new Person(cell.getRow(), cell.getCol(), false, cell.getInjuredPeople().saveInjuredPerson());

                agent.getTask().changeType(TaskType.X_TO_SAFETY, saved);
            // ---------------  If Task Finished ----------------------
            
            
        // Else the task is not done and we have to move to the next cell in the path    
        } else {
            // If the path to the injured person is not found yet we have to search for it
            if (agent.getPath().size() < 1){
                List<CellType> blockers = new ArrayList<>();
                blockers.add(CellType.MOUNTAIN);
                blockers.add(CellType.HOSPITAL);
                blockers.add(CellType.PATH);
                agent.setPath(agent.getGameMap().ruralPathFinding(
                        new Integer[]{agent.getMyCell().getRow(),agent.getMyCell().getCol()},
                        location,
                        blockers, 100)
                );
            }

            assert agent.getPath().size() > 0: "A rural has to save a person that cannot be reached!!!";
            Cell nextCell = agent.getPath().poll();

            try{
                if (nextCell != null )
                    agent.moveToOtherCell(nextCell);
            } catch (Exception e) {
                e.printStackTrace(); assert false;
            }
        }
    }

    private void xToMountainHut(Integer[] location, Person saved) {
        
        // If we are on the mountain hut then the task is done
        if (agent.getMyCell().getCellType() == CellType.MOUNTAIN_HUT){
            // first clear the path since we don't use it anymore
            agent.getPath().clear();
            
            // announce saved person
            agent.getSaved().add(saved);
            
            // change task to none
            agent.getTask().changeType(TaskType.NONE);
        }
        
        // else we have to go a step closer to the mountain hut
        else{
//             if the location of the mountain hut is unknown then search for the nearest mountain hut
           if (location[0] == -1 ){
               Cell mountainHut = agent.getGameMap().getNearestCell(
                                       CellType.MOUNTAIN_HUT,
                                       new Integer[]{agent.getMyCell().getRow(),agent.getMyCell().getCol()}
                                       );
               location[0] = mountainHut.getRow();
               location[1] = mountainHut.getCol();
               agent.getTask().changeType(TaskType.X_TO_SAFETY, saved, location[0], location[1]);
           }
           
            // If the path to the mountain hut is not found yet we have to search for it
            if (agent.getPath().size() < 1){
                List<CellType> blockers = new ArrayList<>();
                blockers.add(CellType.MOUNTAIN);
                blockers.add(CellType.HOSPITAL);
                blockers.add(CellType.PATH);
                agent.setPath(agent.getGameMap().ruralPathFinding(
                        new Integer[]{agent.getMyCell().getRow(),agent.getMyCell().getCol()},
                        location,
                        blockers, 100)
                );
            }
            
            // Move on the path
            
            // if there is no path to the mountain hut, then do nothing
            if (agent.getPath().size() < 1) return;
            
            Cell nextCell = agent.getPath().poll();

            try{
                if (nextCell != null )
                    agent.moveToOtherCell(nextCell);
            } catch (Exception e) {
                e.printStackTrace(); assert false;
            }
        }
    } 
}