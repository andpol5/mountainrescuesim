
package cat.urv.imas.behaviour.rural;

import cat.urv.imas.agent.HelicopterAgent;
import cat.urv.imas.agent.RuralAgent;
import cat.urv.imas.behaviour.helicopter.HelicopterContractNetBehaviour;
import cat.urv.imas.behaviour.helicopter.HelicopterMakeActionBehaviour;
import cat.urv.imas.onthology.MessageContent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;


public class RuralStartStepBehaviour extends OneShotBehaviour{

    @Override
    public void action() {
        RuralAgent agent = (RuralAgent)myAgent;
        
        // Waiting for order from coordinator
        MessageTemplate mt = MessageTemplate.MatchPerformative(MessageContent.START_STEP);
        ACLMessage reply = agent.blockingReceive(mt);
        
        String content = reply.getContent();
        if (content.equals(MessageContent.DO_ACTION))
            agent.addBehaviour(new RuralMakeActionBehaviour());
        else{
            agent.addBehaviour(new RuralVotingBehaviour());
        }
    }
    
}
