
package cat.urv.imas.behaviour.helicopterCoordinator;

import cat.urv.imas.agent.HelicopterCoordinatorAgent;
import cat.urv.imas.onthology.MapArea;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPANames.InteractionProtocol;
;import jade.lang.acl.ACLMessage;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class HelicopterCoordinatorContractNetBehaviour extends OneShotBehaviour {

    @Override
    public void action() {
        HelicopterCoordinatorAgent hcoord = (HelicopterCoordinatorAgent)myAgent;
        
        List<Person> injuredPeople = hcoord.getPeopleToSave();
        
        // First clear the old injured per area map then repopulate it
        for (MapArea area: hcoord.getInjuredPerArea().keySet()){
            hcoord.getInjuredPerArea().get(area).clear();
        }
        for (Person injured : injuredPeople){
            for (MapArea area:hcoord.getInjuredPerArea().keySet()){
                if (area.inArea(injured.getRow(), injured.getCol())){
                    hcoord.getInjuredPerArea().get(area).add(injured);
                    break;
                }
            }
        }
        
        
        // For each area start a contract net for the first injured people in the area
        // If there is none, then start a contract net with the message end contract net
        for (MapArea area: hcoord.getInjuredPerArea().keySet()){
            Person injured = hcoord.getInjuredPerArea().get(area).poll();
                    
            if (injured == null){
                hcoord.runningContractNets += 1;
                ACLMessage newCFP = new ACLMessage(ACLMessage.CFP);
                newCFP.setOntology(area.getId());
                newCFP.setPerformative(ACLMessage.CFP);
                newCFP.setProtocol(InteractionProtocol.FIPA_CONTRACT_NET);
                newCFP.setReplyWith(MessageContent.END_CN);

                hcoord.addBehaviour(new HelicopterCoordinatorContractNetInitiator(newCFP));
            } else {
                hcoord.runningContractNets += 1;
                // otherwise start a new contract net for the next injured person in the area
                ACLMessage newCFP = new ACLMessage(ACLMessage.CFP);
                newCFP.setOntology(area.getId());
                newCFP.setPerformative(ACLMessage.CFP);
                newCFP.setProtocol(InteractionProtocol.FIPA_CONTRACT_NET);
                newCFP.setReplyWith(MessageContent.CONTRACT_NET);
                try{
                    newCFP.setContentObject(injured);
                }catch (IOException e) {hcoord.log(e.toString()); assert false;}

                hcoord.addBehaviour(new HelicopterCoordinatorContractNetInitiator(newCFP));            }
        }
        // if there were no contract nets to be added then add a wait for ready behaviour
        finnish();
        
    }
    
    private void finnish() {
        HelicopterCoordinatorAgent hcoord = (HelicopterCoordinatorAgent)myAgent;
        // if there are no more contract nets then add a wait for ready behaviour
        if (hcoord.runningContractNets < 1){
            hcoord.addBehaviour(new HelicopterCoordinatorWaitForReadyBehaviour());
        }
    }
}
