
package cat.urv.imas.behaviour.helicopterCoordinator;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.HelicopterCoordinatorAgent;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class HelicopterCoordinatorStartStepBehaviour extends OneShotBehaviour{

    @Override
    public void action() {
        
        HelicopterCoordinatorAgent hcoord = (HelicopterCoordinatorAgent)myAgent;
        
        // Wait for gravely injured persons list from system
        MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
        ACLMessage reply = myAgent.blockingReceive(mt);
        
        // Take the ijured people list from the reply
        List<Person> gravelyInjuredPeople = new ArrayList<>();
        try{
            gravelyInjuredPeople = (List<Person>)reply.getContentObject();
        } catch (UnreadableException e){hcoord.log(e.toString()); assert false;}
        
        // Wait for lightly injured people that cannot be saved by rurals
        mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
        reply = myAgent.blockingReceive(mt);
        
        // Take the injured people list from the reply
        List<Person> lightlyInjuredPeople = new ArrayList<>();
        try{
            lightlyInjuredPeople = (List<Person>)reply.getContentObject();
        } catch (UnreadableException e){hcoord.log(e.toString()); assert false;}
        
        // Create one injured people list
        List<Person> injuredPeople = new ArrayList<>();
        injuredPeople.addAll(lightlyInjuredPeople);
        injuredPeople.addAll(gravelyInjuredPeople);
        
        // Check to see if the list of injured persons has changed
        boolean changed = false;
        if (hcoord.getPeopleToSave().size() != injuredPeople.size()) 
            changed = true;
        if (!changed){
            for (Person p:injuredPeople){
                changed = true;
                for (Person o: hcoord.getPeopleToSave()){
                    if (o.equals(p)){
                        changed = false;
                        break;
                    }
                }
                if (changed){
                    break;
                }
            }
        }
        
        // If nothing changed then the helicopters just have to continue with what 
        // they were doing and to wait for the end of turn
        if (!changed){
            for(AID helicopterAgent: hcoord.getHelicopterAgents()) {
                ACLMessage order = new ACLMessage(ACLMessage.REQUEST);
                order.addReceiver(helicopterAgent);
                order.setPerformative(MessageContent.START_STEP);
                order.setContent(MessageContent.DO_ACTION);
                hcoord.log("sending movement request to helicopter " + helicopterAgent.toString());
                hcoord.send(order);
            }
            myAgent.addBehaviour(new HelicopterCoordinatorWaitForReadyBehaviour());
            return;
        }
        
        // if something changed we have to start contract net behaviour and tell al helicopters to start it
        hcoord.setPeopleToSave(injuredPeople);
        for(AID helicopterAgent: hcoord.getHelicopterAgents()) {
                ACLMessage order = new ACLMessage(ACLMessage.REQUEST);
                order.addReceiver(helicopterAgent);
                order.setPerformative(MessageContent.START_STEP);
                order.setContent(MessageContent.CONTRACT_NET);
                hcoord.log("sending movement request to helicopter " + helicopterAgent.toString());
                hcoord.send(order);
        }
        myAgent.addBehaviour(new HelicopterCoordinatorContractNetBehaviour());
    }
    
}
