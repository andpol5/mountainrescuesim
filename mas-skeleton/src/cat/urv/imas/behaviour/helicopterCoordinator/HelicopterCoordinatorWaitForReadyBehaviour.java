
package cat.urv.imas.behaviour.helicopterCoordinator;


import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.HelicopterCoordinatorAgent;
import cat.urv.imas.agent.SystemAgent;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *         The Helicopter coordinator uses this behaviour in order to wait for the helicopter
 *         agents to finish to take actions and to signal Coordinator agent that the they
 *         are ready for the next step.
 */
public class HelicopterCoordinatorWaitForReadyBehaviour extends OneShotBehaviour {

    @Override
    public void action() {
        HelicopterCoordinatorAgent hcoordinator = (HelicopterCoordinatorAgent) myAgent;
        int agentsCount = 0;

        agentsCount += hcoordinator.getHelicopterAgents().size();

        List<Person> saved = new ArrayList<>();
        List<Person> pickedUp = new ArrayList<>();
        try {
            // Wait until all agents have made a move
            for (int i = 0; i < agentsCount; i++) {
                MessageTemplate mt = MessageTemplate.MatchPerformative(MessageContent.READY);
                ACLMessage reply = myAgent.blockingReceive(mt);
                String otherInfo = reply.getReplyWith();
                
                
                Object content = reply.getContentObject();
                if(content != null) {
                    List<Person> savedByAgent = (List<Person>) content;
                    
                    if ((otherInfo == null) ? true : otherInfo.equals(MessageContent.SAVED)){
                        saved.addAll(savedByAgent);
                    } else {
                        pickedUp.addAll(savedByAgent);
                    }
                }
                
            }
        } catch (UnreadableException e) {
            e.printStackTrace();
        }
        
        UtilsAgents.sendReady(hcoordinator, AgentType.COORDINATOR, (Serializable) saved);
        UtilsAgents.sendReady(hcoordinator, AgentType.COORDINATOR, (Serializable) pickedUp);
        hcoordinator.addBehaviour(new HelicopterCoordinatorStartStepBehaviour());
    }
}
