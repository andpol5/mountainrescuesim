
package cat.urv.imas.behaviour.helicopterCoordinator;

import cat.urv.imas.agent.HelicopterAgent;
import cat.urv.imas.agent.HelicopterCoordinatorAgent;
import cat.urv.imas.onthology.MapArea;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import jade.proto.ContractNetInitiator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HelicopterCoordinatorContractNetInitiator extends OneShotBehaviour{
    
    ACLMessage cfp;
    Map<AID, ACLMessage> responses;
    MapArea onthology;
    
    public HelicopterCoordinatorContractNetInitiator(ACLMessage cfp) {
        super();
        this.cfp = cfp;
        responses = new HashMap<>();
    }
    
    @Override
    public void action() {
        HelicopterCoordinatorAgent hcoord = (HelicopterCoordinatorAgent)myAgent;
        // The call For Proposals
        makeCall(cfp);
        
        
            waitForResponses();

        if (cfp.getReplyWith() == null ? true : !cfp.getReplyWith().equals(MessageContent.END_CN)){
            // PREPEARE ANSWERS
            Map<AID, ACLMessage> answers = dealWithResponses();

            sendAnswers(answers);
        } else {
            hcoord.runningContractNets -= 1;
            finnish();
        }
    }


    private void finnish() {
        HelicopterCoordinatorAgent hcoord = (HelicopterCoordinatorAgent)myAgent;
        // if there are no more contract nets then add a wait for ready behaviour
        if (hcoord.runningContractNets < 1){
            hcoord.addBehaviour(new HelicopterCoordinatorWaitForReadyBehaviour());
        }
    }
    
    private void makeCall(ACLMessage cfp){
        HelicopterCoordinatorAgent hcoord = (HelicopterCoordinatorAgent)myAgent;
        
        // We send a message to all the helicopters
        for (AID helicopter:hcoord.getHelicopterAgents()){
            cfp.addReceiver(helicopter);
        }
        
        hcoord.send(cfp);
    }

    private void waitForResponses() {
        HelicopterCoordinatorAgent hcoord = (HelicopterCoordinatorAgent)myAgent;
        responses.clear();
        
        // Wait for an refusal or proposal from all helicopters
        MapArea area = hcoord.getAreaByID(cfp.getOntology());
        for (AID helicopter: hcoord.getHelicoptersPerArea().get(area)){
            MessageTemplate mt = MessageTemplate.and(
                        MessageTemplate.or(
                                MessageTemplate.MatchPerformative(ACLMessage.PROPOSE),
                                MessageTemplate.MatchPerformative(ACLMessage.REFUSE)
                        ), MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET)
            );

            ACLMessage response = hcoord.blockingReceive(mt);
            responses.put(response.getSender(), response);
        }
    }

    private Map<AID, ACLMessage> dealWithResponses() {
        HelicopterCoordinatorAgent hcoord = (HelicopterCoordinatorAgent)myAgent;
        hcoord.runningContractNets -= 1;
        Map<AID, ACLMessage> acceptances = new HashMap<>();
        
        List<AID> potentialContractors = new ArrayList <>();
                
        for (AID i:responses.keySet()){
            ACLMessage response = responses.get(i);
            if (response.getPerformative() == ACLMessage.PROPOSE){
                potentialContractors.add(i);
            }
        }
        
        // If there proposals then prepare the acceptances
        if (potentialContractors.size() > 0){

            // get the injured person to be saved. All messages should have the same person as content object
            Person injured = new Person();
            ACLMessage someMessage = responses.values().iterator().next();
            try{
                injured = (Person)someMessage.getContentObject();
            } catch(UnreadableException e) {e.printStackTrace();assert false;}

            // find the best proposal
            AID bestProposal = null; 
            int bestNbOfSteps = Integer.MAX_VALUE;
            for (AID i : potentialContractors){
                int proposedNbSteps = Integer.parseInt(responses.get(i).getReplyWith());

                if (proposedNbSteps < bestNbOfSteps){
                    bestNbOfSteps = proposedNbSteps;
                    bestProposal = i;
                }
            }

            // If the best proposed number of steps is over the steps to die of the 
            // injured person then all the acceptances will be refuse
            if (injured.getStepToDie() < bestNbOfSteps){
                bestProposal = null;
            }

            // Preparing all responses. All responses will be refuse except the one that 
            // is marked as best proposal
            for (AID i: potentialContractors){
                if (i == bestProposal){
                    acceptances.put(i, new ACLMessage(ACLMessage.ACCEPT_PROPOSAL));
                } else{
                    acceptances.put(i, new ACLMessage(ACLMessage.REJECT_PROPOSAL));
                }
            }
        }
        // now initiate another contract net for the nex person or to transmit that the contract net is over
        
        // find the area
        String areaID = cfp.getOntology();
        onthology = hcoord.getAreaByID(areaID);
        
        assert !onthology.getId().equals("-1") : "Something went wrong when searching for the area of the injured person";
        
        // get the next injured person in the area
        Person nextInjured = hcoord.getInjuredPerArea().get(onthology).poll();
        
        // if there is no other injured person then start a contract net with the message
        // contract net end
        if (nextInjured == null){
            ACLMessage newCFP = new ACLMessage(ACLMessage.CFP);
            newCFP.setOntology(areaID);
            newCFP.setPerformative(ACLMessage.CFP);
            newCFP.setProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET);
            newCFP.setReplyWith(MessageContent.END_CN);
            
            // We add the initiator behaviour
            hcoord.addBehaviour(new HelicopterCoordinatorContractNetInitiator(newCFP));
            hcoord.runningContractNets += 1;
        } else {
            // otherwise start a new contract net for the next injured person in the area
            ACLMessage newCFP = new ACLMessage(ACLMessage.CFP);
            newCFP.setOntology(areaID);
            newCFP.setPerformative(ACLMessage.CFP);
            newCFP.setProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET);
            newCFP.setReplyWith(MessageContent.CONTRACT_NET);
            try{
                newCFP.setContentObject(nextInjured);
            }catch (IOException e) {hcoord.log(e.toString()); assert false;}
            
            // We add the initiator behaviour
            hcoord.addBehaviour(new HelicopterCoordinatorContractNetInitiator(newCFP));
            hcoord.runningContractNets += 1;
        }
        
        finnish();
        
        return acceptances;
    }

    private void sendAnswers(Map<AID, ACLMessage> answers) {
        for (AID i : answers.keySet()){
            ACLMessage m = answers.get(i);
            m.setOntology(cfp.getOntology());
            m.setProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET);
            m.clearAllReceiver();
            m.addReceiver(i);
            myAgent.send(m);
        }
    }


        
}
