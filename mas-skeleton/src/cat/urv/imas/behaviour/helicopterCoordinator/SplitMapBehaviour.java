
package cat.urv.imas.behaviour.helicopterCoordinator;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.HelicopterCoordinatorAgent;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.CellType;
import cat.urv.imas.onthology.HelicopterStartDescription;
import cat.urv.imas.onthology.MapArea;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

/**
 *The map is going to be slpit in a few rectangles (that will be the shape of all areas).
 * 
 * An area is always a rectangle and thus can be defined by 4 values: highest row,
 * lowest row, rightmost column and leftmost column. We decided that the values retained 
 * will be inclusive (i.e. if one areas limit is row 2, then cell 2,4 may be in that area)
 */
public class SplitMapBehaviour extends OneShotBehaviour {

    @Override
    public void action() {
        HelicopterCoordinatorAgent hcoord = (HelicopterCoordinatorAgent)myAgent;
        
        // Splitting map to areas
        List<Cell> hospitals = hcoord.getMap().getCells(CellType.HOSPITAL);
        List<MapArea> areas = splitMap(hospitals);
        
        // initialize injuredPerArea
        for (MapArea area: areas){
            hcoord.getInjuredPerArea().put(area, new PriorityQueue<>(10,
                    new Comparator<Person>(){
                        @Override
                        public int compare(Person o1, Person o2) {
                            return Integer.compare(o1.getStepToDie(), o2.getStepToDie());
                        }
                    }
            ));
        }
                
        List<HelicopterStartDescription> helicopters = new ArrayList<>();
        //------ Wait for helicopters to send the start location------------------------
        int agentsCount = 0;
        agentsCount += hcoord.getMap().howManyAgents(AgentType.HELICOPTER);
        for (int i = 0; i<agentsCount; i++){
            MessageTemplate mt = MessageTemplate.MatchPerformative(MessageContent.READY);
            ACLMessage reply = myAgent.blockingReceive(mt);
            
            try{
                HelicopterStartDescription helicopter = (HelicopterStartDescription)reply.getContentObject();
                helicopters.add(helicopter);
            } catch (UnreadableException e) { e.printStackTrace(); assert false;}
        }
        // --------------------------------------------------------
        
        hcoord.setHelicoptersPerArea(linkHelicoptersToArea(areas,helicopters));
        
        // Now send to the helicopters their areas
        for (MapArea area: hcoord.getHelicoptersPerArea().keySet()){
            for (AID helicopter: hcoord.getHelicoptersPerArea().get(area)){
                ACLMessage areaMessage = new ACLMessage(ACLMessage.INFORM);
                try{
                    areaMessage.setContentObject(area);
                    areaMessage.clearAllReceiver();
                    areaMessage.addReceiver(helicopter);
                    hcoord.send(areaMessage);
                } catch(IOException e) {hcoord.log(e.toString()); assert false;}
            }
        }
        
        UtilsAgents.sendReady(hcoord, AgentType.COORDINATOR, null);
        UtilsAgents.sendReady(hcoord, AgentType.COORDINATOR, null);
        hcoord.addBehaviour(new HelicopterCoordinatorStartStepBehaviour());
    
    }

    private boolean checkOneRow(List<Cell> hospitals) {
        int row = hospitals.get(0).getRow();
        for (Cell hospital:hospitals){
            if (row != hospital.getRow())
                return false;
        }
        return true;
    }

    private List<MapArea> splitMap(List<Cell> hospitals) {
        HelicopterCoordinatorAgent hcoord = (HelicopterCoordinatorAgent)myAgent;
        
        List<MapArea> areas = new ArrayList<>();
        
        // ordering hospital cells by row (from top to bottom of the map)
        Collections.sort(hospitals, new Comparator<Cell>(){
            @Override
            public int compare(Cell o1, Cell o2) {
                if (o1.getRow()> o2.getRow()){
                    return 1;
                }
                if (o1.getRow() < o2.getRow()){
                    return -1;
                }
                return 0;
            }
            
        });
        
        // Setting first extremes
        int size = hospitals.size();
        int minrow = getExtreme(hcoord.getMap().getCells(),"row","min");
        int maxrow = getExtreme(hcoord.getMap().getCells(),"row","max");
        int mincol = getExtreme(hcoord.getMap().getCells(),"col","min");
        int maxcol = getExtreme(hcoord.getMap().getCells(),"col","max");
        
        // Splitting the map
        for (int i = 0; i < size; i++){
            // If there is one hospital left then return the remaining part of the map
            if (size - i == 1){
                areas.add(new MapArea(minrow, maxrow, mincol, maxcol));
                break;
            }
            
            // If the remaining hospitals are on one row than split them in 2 areas
            // by column then break
            if (checkOneRow(hospitals.subList(i, size))){
                MapArea m1 = new MapArea(minrow, maxrow, mincol, hospitals.get(i).getCol());
                MapArea m2 = new MapArea(minrow, maxrow, (hospitals.get(i).getCol() + 1), maxcol);
                areas.add(m1);
                areas.add(m2);
                break;
            }
            
            // If the rest of the hospital except the current one are in one row,
            // then make a map area only with the current hospital
            if (checkOneRow(hospitals.subList(i + 1, size))){
                areas.add(new MapArea(minrow, hospitals.get(i).getRow(), mincol, maxcol));
                // update minrow
                minrow = hospitals.get(i).getRow() +1;
                continue;
            }
            
            // Else create a map area going from the last minrow to the next hospitals
            // row. This might include other hospitals on the same row as the next hospital
            // so we need to make sure to update i to the number of hospitals in the new area
            MapArea area = new MapArea(minrow, hospitals.get(i + 1).getRow(), mincol, maxcol);
            
            // increasing the index to skip the hospitals already included
            for (Cell hospital:hospitals.subList(i+1, size)){
                if (area.inArea(hospital.getRow(), hospital.getCol())){
                    i++;
                }
            }
            
        }
        
        return areas;
    }

    private Map<MapArea, List<AID>> linkHelicoptersToArea(List<MapArea> areas, List<HelicopterStartDescription> helicopters) {
        // initializing the map
        Map<MapArea, List<AID>> helicoptersPerArea = new HashMap<>();
        for (MapArea area  : areas){
            helicoptersPerArea.put(area, new ArrayList<AID>());
        }
        
        for (HelicopterStartDescription helicopter: helicopters){
            for (MapArea area:areas){
                if (area.inArea(helicopter.row, helicopter.col)){
                    helicoptersPerArea.get(area).add(helicopter.aid);
                }
            }
        }
        
        return helicoptersPerArea;
    }
    
    public int getExtreme(List<Cell> cells,String rowOrCol, String minOrMax){
        double extreme = 0;
        boolean maxim = true;
        if(minOrMax.equals("min")){
            extreme = Double.POSITIVE_INFINITY;
            maxim = false;
        }
        for (Cell cell:cells){
            if (rowOrCol.equals("row")){
                if (maxim){ if (extreme < cell.getRow()) extreme = cell.getRow();}
                else if (extreme > cell.getRow()) extreme = cell.getRow();
            } else {
                if (maxim) { if (extreme < cell.getCol()) extreme = cell.getCol();}
                else if (extreme > cell.getCol()) extreme = cell.getCol();
            }
        }
        return (int)extreme;
    }
}
