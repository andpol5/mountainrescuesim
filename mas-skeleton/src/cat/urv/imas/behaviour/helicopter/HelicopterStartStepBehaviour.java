
package cat.urv.imas.behaviour.helicopter;

import cat.urv.imas.agent.HelicopterAgent;
import cat.urv.imas.agent.MoveableAgent;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import cat.urv.imas.onthology.TaskType;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;


public class HelicopterStartStepBehaviour extends OneShotBehaviour {

    @Override
    public void action() {
        HelicopterAgent agent = (HelicopterAgent)myAgent;
        
        // Waiting for order from coordinator
        MessageTemplate mt = MessageTemplate.MatchPerformative(MessageContent.START_STEP);
        ACLMessage reply = myAgent.blockingReceive(mt);
        
        String content = reply.getContent();
        if (content.equals(MessageContent.DO_ACTION))
            myAgent.addBehaviour(new HelicopterMakeActionBehaviour());
        else{
            agent.setReadyForCN(false);
            myAgent.addBehaviour(new HelicopterContractNetBehaviour());
        }
    }
    
}
