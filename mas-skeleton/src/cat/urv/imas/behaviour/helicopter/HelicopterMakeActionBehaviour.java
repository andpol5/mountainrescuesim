
package cat.urv.imas.behaviour.helicopter;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.HelicopterAgent;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.CellType;
import cat.urv.imas.map.PathCell;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import cat.urv.imas.onthology.Task;
import cat.urv.imas.onthology.TaskType;
import jade.core.behaviours.OneShotBehaviour;
import jade.tools.sniffer.Message;
import java.io.Serializable;
import java.util.ArrayList;


public class HelicopterMakeActionBehaviour extends OneShotBehaviour {
    
    HelicopterAgent agent;

    @Override
    public void action() {
        agent = (HelicopterAgent)myAgent;
        if(!switchAction(agent.getTask1(), 1))
            if(!switchAction(agent.getTask2(),2))
                goIdle(new Integer[]{-1,-1});
        
        agent.setReadyForCN(false);
        if (agent.getPickedUp().size() > 0){
            UtilsAgents.sendReady(agent, AgentType.HELICOPTER_COORDINATOR, (Serializable)agent.getPickedUp(),
                                    MessageContent.PICK_UP);
        } else {
            UtilsAgents.sendReady(agent, AgentType.HELICOPTER_COORDINATOR, (Serializable)agent.getSaved(),
                                    MessageContent.SAVED);
        }
        agent.getSaved().clear();
        agent.getPickedUp().clear();
        agent.addBehaviour(new HelicopterStartStepBehaviour());
    }
    
    private boolean switchAction(Task currentTask, int taskno) {
        
        switch(currentTask.getType()){
            case GO_IDLE:
                goIdle(new Integer[]{currentTask.getRow(),currentTask.getCol()});
                return true;
            case SAVE_X:
                saveX(new Integer[]{currentTask.getRow(),currentTask.getCol()}, taskno);
                return true;
            case X_TO_SAFETY:
                xToHospital(new Integer[]{currentTask.getRow(),currentTask.getCol()}, taskno, currentTask.getSaved());
                return true;
            case NONE:
                return false;
        }
        
        return false;
    }

    private void goIdle(Integer[] location) {
        
        // if it is already on an hospital then it has nothing to do this turn
        if (agent.getMyCell().getCellType() == CellType.HOSPITAL){
            // Make sure that the path is empty
            agent.getPath().clear();
            return;
        }
        
        // if the location of the hospital is unknown then search for the nearest hospital in the area
        if (location[0] == -1 ){
            Cell hospital = agent.getGameMap().getNearestCell(
                                    CellType.HOSPITAL,
                                    new Integer[]{agent.getMyCell().getRow(),agent.getMyCell().getCol()},
                                    agent.getArea()
                                    );
            location[0] = hospital.getRow();
            location[1] = hospital.getCol();
            agent.setPath(agent.getGameMap().helicopterPathFinding(
                            new Integer[]{agent.getMyCell().getRow(),agent.getMyCell().getCol()},
                            location ,
                            new ArrayList<CellType>(){}));
        }
        
        
        assert agent.getPath().size() > 0: "There is no path for a helicopter!!!";
        Cell nextCell = agent.getPath().poll();
        
        // Since the area is a rectangle it is sure that the shortest path between
        // to points in the area will be inside the area
        try{
            agent.moveToOtherCell(nextCell);
        } catch (Exception e) {e.printStackTrace(); assert false;}
        
    }

    private void saveX(Integer[] location, int taskno) {
        assert location[0] > -1 && location[1] > -1: "Unknown location of the person to be saved!";
        
        // If we are on the cell with the person to be saved or on an adjacent cell
        // then the task is done and we save the person and change the task to x to hospital
        boolean sameCell = (location[0] == agent.getMyCell().getRow()) && 
                           (location[1] == agent.getMyCell().getCol());
        boolean adjacent = agent.getMyCell().isAdjacent(location);
        
        if (sameCell || adjacent){
            
            // ---------------  If Task Finished ----------------------
            // clear the path since we will not use it anymore
            agent.getPath().clear();

            
            PathCell cell = (PathCell)agent.getGameMap().getMap()[location[0]][location[1]];
            Person saved = new Person(cell.getRow(), cell.getCol(), false, cell.getInjuredPeople().saveInjuredPerson());
            
            // announce picked up
            agent.getPickedUp().add(saved);
            
            if (taskno == 1){
                // if the second task is another save x then we need to change order so the save x
                // will be performed before
                if (agent.getTask2().getType() == TaskType.SAVE_X){
                    int t2row = agent.getTask2().getRow();
                    int t2col = agent.getTask2().getCol();
                    agent.getTask1().changeType(TaskType.SAVE_X, t2row, t2col);
                    agent.getTask2().changeType(TaskType.X_TO_SAFETY, saved);
                } else {
                    agent.getTask1().changeType(TaskType.X_TO_SAFETY, saved);
                }
            }
            else{
                agent.getTask2().changeType(TaskType.X_TO_SAFETY, saved);
            }
            // ---------------  If Task Finished ----------------------
            
            
        // Else the task is not done and we have to move to the next cell in the path    
        } else {
            // If the path to the injured person is not found yet we have to search for it
            if (agent.getPath().size() < 1){
                agent.setPath(agent.getGameMap().helicopterPathFinding(
                        new Integer[]{agent.getMyCell().getRow(),agent.getMyCell().getCol()},
                        location,
                        new ArrayList<CellType>(){})
                );
            }

            assert agent.getPath().size() > 0: "There is no path for a helicopter!!!";
            Cell nextCell = agent.getPath().poll();

            // Since the area is a rectangle it is sure that the shortest path between
            // two points in the area will be inside the area
            try{
                agent.moveToOtherCell(nextCell);
            } catch (Exception e) {e.printStackTrace(); assert false;}
        }
    }

    private void xToHospital(Integer[] location, int taskno, Person saved) {
        
        // If we are on the hospital then the task is done
        if (agent.getMyCell().getCellType() == CellType.HOSPITAL){
            // first clear the path since we don't use it anymore
            agent.getPath().clear();
            
            // announce saved person
            agent.getSaved().add(saved);
            
            // change task to none
            if (taskno == 1) { agent.getTask1().changeType(TaskType.NONE);
            } else { agent.getTask2().changeType(TaskType.NONE);}
        }
        
        // else we have to go a step closer to the hospital
        else{
            // if the location of the hospital is unknown then search for the nearest hospital in the area
           if (location[0] == -1 ){
               Cell hospital = agent.getGameMap().getNearestCell(
                                       CellType.HOSPITAL,
                                       new Integer[]{agent.getMyCell().getRow(),agent.getMyCell().getCol()},
                                       agent.getArea()
                                       );
               location[0] = hospital.getRow();
               location[1] = hospital.getCol();
               if (taskno == 1) agent.getTask1().changeType(TaskType.X_TO_SAFETY, agent.getTask1().getSaved(),
                                                            location[0],location[1]);
               if (taskno == 2) agent.getTask2().changeType(TaskType.X_TO_SAFETY, agent.getTask2().getSaved(),
                                                            location[0],location[1]);
               
               
           }
           
            // If the path to the hospital is not found yet we have to search for it
            if (agent.getPath().size() < 1){
                agent.setPath(agent.getGameMap().helicopterPathFinding(
                        new Integer[]{agent.getMyCell().getRow(),agent.getMyCell().getCol()},
                        location,
                        new ArrayList<CellType>(){})
                );
            }
            
            // Move on the path
            assert agent.getPath().size() > 0: "There is no path for a helicopter!!!";
            Cell nextCell = agent.getPath().poll();

            // Since the area is a rectangle it is sure that the shortest path between
            // two points in the area will be inside the area
            try{
                agent.moveToOtherCell(nextCell);
            } catch (Exception e) {e.printStackTrace(); assert false;}
        }
    }    
    
}
