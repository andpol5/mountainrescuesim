
package cat.urv.imas.behaviour.helicopter;

import cat.urv.imas.agent.HelicopterAgent;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.TaskType;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;


public class HelicopterContractNetBehaviour extends OneShotBehaviour {

    @Override
    public void action() {
        HelicopterAgent helicopter = (HelicopterAgent)myAgent;

            // Before the contract net the helicopter has to renounce to other types 
            // of tasks other than x top hospital

            helicopter.getPath().clear();
            
            if (helicopter.getTask1().getType() != TaskType.X_TO_SAFETY){
                helicopter.getTask1().changeType(TaskType.NONE);
            } else if(helicopter.getTask2().getType() != TaskType.X_TO_SAFETY){
                // we need to change the order
                helicopter.getTask2().changeType(TaskType.X_TO_SAFETY, helicopter.getTask2().getSaved());
                helicopter.getTask1().changeType(TaskType.NONE);
            }
            if (helicopter.getTask2().getType() != TaskType.X_TO_SAFETY){
                helicopter.getTask2().changeType(TaskType.NONE);
            }
            
            helicopter.setReadyForCN(true);

    }
    
}
