
package cat.urv.imas.behaviour.helicopter;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.HelicopterAgent;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.CellType;
import cat.urv.imas.map.InjuredPeople;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import cat.urv.imas.onthology.Task;
import cat.urv.imas.onthology.TaskType;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import jade.proto.ContractNetResponder;
import jade.proto.SSContractNetResponder;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;


public class HelicopterContractNetResponder extends TickerBehaviour{
    
    ACLMessage cfp;
    MessageTemplate cfpTemplagte;
    
    public HelicopterContractNetResponder(Agent a, MessageTemplate cfpTemplagte) {
        super(a, 500);
        this.cfpTemplagte = cfpTemplagte;
    }
    

    private void checkCall() {
        HelicopterAgent helicopter = (HelicopterAgent)myAgent;
        
        if (helicopter.isReadyForCN()){
            cfp = helicopter.receive(cfpTemplagte);
            if ((cfp == null)){
                return;
            }
        } else {return;}
       
                
        // Process the call for proposals and prepare a response
        ACLMessage response = prepareResponse();
        
        // Send response back to the initiator
        sendResponse(response);
    }

    private void sendResponse(ACLMessage response) {
        HelicopterAgent helicopter = (HelicopterAgent)myAgent;
        
        // preparing the response
        response.setOntology(helicopter.getArea().getId());
        response.setProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET);
        
        ServiceDescription searchCriterion = new ServiceDescription();
        searchCriterion.setType(AgentType.HELICOPTER_COORDINATOR.toString());
        AID toAgent = UtilsAgents.searchAgent(helicopter, searchCriterion);
        
        response.addReceiver(toAgent);
        
        // send the response
        helicopter.send(response);
        
        // if the response was a refusal then return, otherwise wait for a response
        if (response.getPerformative() == ACLMessage.REFUSE)
            return;
        
        MessageTemplate mt = MessageTemplate.and(
            MessageTemplate.or(
                    MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL),
                    MessageTemplate.MatchPerformative(ACLMessage.REJECT_PROPOSAL)
            ),
            MessageTemplate.and(
                    MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET),
                    MessageTemplate.MatchOntology(helicopter.getArea().getId())
            )
        );
        ACLMessage reply = helicopter.blockingReceive(mt);
        
        if (reply.getPerformative() == ACLMessage.ACCEPT_PROPOSAL){
            handleAcceptProposal();
        }
        
}

    private ACLMessage prepareResponse() {       
        HelicopterAgent helicopter = (HelicopterAgent)myAgent;
        
        // If a message has been recieved then continue with the behaviour
        String content = cfp.getReplyWith();

        // If the contract net ended in the area then add make action behaviour
        if (content.equals(MessageContent.END_CN)){
            ACLMessage response =  new ACLMessage(ACLMessage.REFUSE);
            myAgent.addBehaviour(new HelicopterMakeActionBehaviour());
            return response;
        }
        
        
        // Else it has to get the person that needs to be saved and return an acl
        // message that send the number of steps
        Person injured = new Person();
        try{
            injured = (Person)cfp.getContentObject();
        } catch(UnreadableException e){ helicopter.log(e.toString()); assert false;}
        
        // If the helicopter already has 2 tasks then he must refuse
        if (helicopter.isBusy()){
            ACLMessage response =  new ACLMessage(ACLMessage.REFUSE);
            try { response.setContentObject(injured);}catch(IOException e){e.printStackTrace(); assert false;}
            return response;
        }

        // Check if there is another save x task and, if so, get the distance
        int distance = 0;
        Task otherSaveXTask = null;
        if (helicopter.getTask1().getType() == TaskType.SAVE_X){
            otherSaveXTask = helicopter.getTask1();
            Queue<Cell> otherpath = helicopter.getGameMap().helicopterPathFinding(
                new Integer[]{helicopter.getMyCell().getRow(),helicopter.getMyCell().getCol()},
                new Integer[]{helicopter.getTask1().getRow(),helicopter.getTask1().getCol()},
                new ArrayList<CellType>(){});
            distance = otherpath.size() - 1;
        } else if (helicopter.getTask2().getType() == TaskType.SAVE_X){
            otherSaveXTask = helicopter.getTask2();
            Queue<Cell> otherpath = helicopter.getGameMap().helicopterPathFinding(
                new Integer[]{helicopter.getMyCell().getRow(),helicopter.getMyCell().getCol()},
                new Integer[]{helicopter.getTask2().getRow(),helicopter.getTask2().getCol()},
                new ArrayList<CellType>(){});
            distance = otherpath.size() - 1;
        }
        
        // Set up the starting point. The location of the helicopter if there is no
        // other save x task, but if there is then from the other persons location
        Integer[] startLocation = new Integer[]{injured.getRow(),injured.getCol()};
        if (otherSaveXTask != null){
            startLocation[0] = otherSaveXTask.getRow();
            startLocation[1] = otherSaveXTask.getCol();
        }
        
        // Get the path to that person 
        Queue<Cell> path = helicopter.getGameMap().helicopterPathFinding(
                new Integer[]{helicopter.getMyCell().getRow(),helicopter.getMyCell().getCol()},
                startLocation,
                new ArrayList<CellType>(){});
        
        ACLMessage response = new ACLMessage(ACLMessage.PROPOSE);
        distance = path.size() - 1 + distance;
        String d = Integer.toString(distance);
        response.setReplyWith(d);
        try { response.setContentObject(injured);}catch(IOException e){e.printStackTrace(); assert false;}
        return response;
    }
    
    protected void handleAcceptProposal(){
        HelicopterAgent helicopter = (HelicopterAgent)myAgent;
        
        // get the injured person
        Person injured = new Person();
        try{
             injured = (Person)cfp.getContentObject();
        } catch (UnreadableException e){helicopter.log(e.toString()); assert false;}
        
        if(helicopter.getTask1().getType() == TaskType.NONE)
            helicopter.getTask1().changeType(TaskType.SAVE_X, injured.getRow(), injured.getCol());
        else
            helicopter.getTask2().changeType(TaskType.SAVE_X, injured.getRow(), injured.getCol());

    }

    @Override
    protected void onTick() {
        // Check if a call for proposals has been made
        checkCall();    
    }
}
