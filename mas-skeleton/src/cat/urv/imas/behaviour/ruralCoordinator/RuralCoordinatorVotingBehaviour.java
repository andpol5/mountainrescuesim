
package cat.urv.imas.behaviour.ruralCoordinator;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.RuralCoordinatorAgent;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.map.InjuredPeople;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Pair;
import cat.urv.imas.onthology.Person;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class RuralCoordinatorVotingBehaviour extends OneShotBehaviour {

    RuralCoordinatorAgent rcoord;
    
    @Override
    public void action() {
        rcoord = (RuralCoordinatorAgent)myAgent;
        
        // Send Injured people list to the rurals
        for(AID ruralAgent: rcoord.getRuralAgents()) {
            ACLMessage list = new ACLMessage(ACLMessage.INFORM);
            list.addReceiver(ruralAgent);
            list.setProtocol(MessageContent.VOTING);

            try{
                list.setContentObject((Serializable)rcoord.getInjuredPeople());
            }catch (IOException e){rcoord.log(e.toString()); assert false;}

            rcoord.send(list);
        }
        
        // Wait for votes form rurals
        Map<AID, List<Pair<Person, Integer>>> votesPerRural = waitForVotes();
        
        // process the votes in order to extract which rural svaes which person
        List<Pair<AID,Person>> conclusion = processVotes(votesPerRural);
        
        // send the information back to the agents
        sendConclusions(conclusion);
        
        // Add a wait for ready behaviour
        rcoord.addBehaviour(new RuralCoordinatorWaitForReadyBehaviour());
        
        
    }

    private Map<AID, List<Pair<Person, Integer>>> waitForVotes() {
        Map<AID, List<Pair<Person, Integer>>> votePerRural = new HashMap<>();
        
        for(AID ruralAgent: rcoord.getRuralAgents()) {
            MessageTemplate vt =  MessageTemplate.and(
                            MessageTemplate.MatchPerformative(MessageContent.VOTE),
                            MessageTemplate.MatchProtocol(MessageContent.VOTING)
                            );
            ACLMessage voteMessage = rcoord.blockingReceive(vt);
            
            try{
                List<Pair<Person, Integer>> vote = (List<Pair<Person, Integer>>) voteMessage.getContentObject();
                // Eliminate from the map the rurals that are busy (they sent null)
                if (vote != null){
                    votePerRural.put(voteMessage.getSender(), vote);
                }
            }catch (UnreadableException e){rcoord.log(e.toString()); assert false;}
        }
        
        return votePerRural;
    }

    private List<Pair<AID, Person>> processVotes(Map<AID, List<Pair<Person, Integer>>> votesPerRural) {
        List<Pair<AID, Person>> conclusions = new ArrayList<>();
        
        List<AID> possibleAID = new ArrayList<>(votesPerRural.keySet());
        List<AID> extractedAID = new ArrayList<>();
        
        List<Person> possiblePersons = new ArrayList<>();
        for (AID aid : votesPerRural.keySet()){
            for (Pair<Person, Integer> pair : votesPerRural.get(aid)){
                if (!possiblePersons.contains(pair.getKey())){
                    possiblePersons.add(pair.getKey());
                }
            }
        }
        List<Person> extractedPersons = new ArrayList<>();
        
        int nbPersons = possiblePersons.size();
        
        for (int i = 0; i < nbPersons; i++){
            
            // Extracting the winning rurals on this level
            List<AID> extractedAIDTemp = new ArrayList<>();
            List<Person> extractedPersonsTemp = new ArrayList<>();
            
            
            List<Person> doublesOnLevel = new ArrayList<>();
            for (AID aid: votesPerRural.keySet()){
                for (AID aid2: votesPerRural.keySet()){
                    if (!aid2.equals(aid)){
                        Person p1 = votesPerRural.get(aid).get(i).getKey();
                        Person p2 = votesPerRural.get(aid2).get(i).getKey();
                        if (p1.equals(p2)){
                            if (!doublesOnLevel.contains(p1)){
                                doublesOnLevel.add(p1);
                                break;
                            }
                        }
                    }
                }

            }
            
            for (AID aid: votesPerRural.keySet()){
                Person p = votesPerRural.get(aid).get(i).getKey();
                if (!doublesOnLevel.contains(p) && !extractedPersons.contains(p)){
                    if (votesPerRural.get(aid).get(i).getValue() < Integer.MAX_VALUE){
                        extractedAIDTemp.add(aid);
                        extractedPersonsTemp.add(p);
                    }
                }
            }
            
            // Adding to conclusion the winning rurals
            for (AID aid:extractedAIDTemp){
                Person p = votesPerRural.get(aid).get(i).getKey();
                conclusions.add(new Pair<>(aid, p));
            }
            extractedAID.addAll(extractedAIDTemp);
            
            // Solving conflicts on the level
            ArrayList<Person> t = (ArrayList < Person >)doublesOnLevel;
            List<Person>nonExtractedDoubles = (List < Person >)t.clone();
            nonExtractedDoubles.removeAll(extractedPersonsTemp);
            
            // Extracting as conclusion the rural with the least number of steps 
            // if it is the only one with that number if steps per person
            for (Person p : nonExtractedDoubles){
                AID rural = new AID();
                int minSteps = Integer.MAX_VALUE;
                boolean isonly = true;
                for (AID aid: votesPerRural.keySet()){
                    Person p2 = votesPerRural.get(aid).get(i).getKey();
                    if (p2.equals(p)){
                        int steps = votesPerRural.get(aid).get(i).getValue();
                        if (minSteps > steps){
                            minSteps = steps;
                            rural = aid;
                            isonly = true;
                        } else if (minSteps == steps){
                            isonly = false;
                        }
                    }
                }
                if (isonly && minSteps < Integer.MAX_VALUE){
                    conclusions.add(new Pair<>(rural, p));
                    extractedAID.add(rural);
                    extractedPersons.add(p);
                }
            }            
        }
        
        // There may be some rurals that are not extracted (i.e. do not have a person to save)
        possibleAID.removeAll(extractedAID);
        
        if (possibleAID.size() > 0){
            // For these rurals we assciate a random person
            possiblePersons.removeAll(extractedPersons);
            
            extractedAID.clear();
            for (AID aid : possibleAID){
                // if there are no more persons then associate null
                if (possiblePersons.size() < 1){
                    conclusions.add(new Pair<>(aid, (Person)(null)));
                    extractedAID.add(aid);
                } else {
                    for (Person p: possiblePersons){
                        int index = votesPerRural.get(aid).indexOf(p);
                        if (index >= 0){
                            if (votesPerRural.get(aid).get(index).getValue() < Integer.MAX_VALUE){
                                conclusions.add(new Pair<>(aid, possiblePersons.remove(0)));
                                extractedAID.add(aid);
                            }
                        }
                    }
                }
            }
            
            // If there still are agents then they have nothing to do
             possibleAID.removeAll(extractedAID);
             if (possibleAID.size() > 0){
                  for (AID aid : possibleAID){
                      conclusions.add(new Pair<>(aid, (Person)(null)));
                  }
             }
        }
        
        // if there are still persons that are not extracted that means that there
        // is no rural that can save them so we have to send them back to the coordianator
        // if not we will send the empty list
        sendBack(possiblePersons);
        
        return conclusions;
    }

    private void sendBack(List<Person> injuredPeople) {
        // Search coordinator
        ServiceDescription searchCriterionRuralCoordinator = new ServiceDescription();
        searchCriterionRuralCoordinator.setType(AgentType.COORDINATOR.toString());
        AID coordinator = UtilsAgents.searchAgent(myAgent, searchCriterionRuralCoordinator);
        
        ACLMessage message = new ACLMessage(ACLMessage.INFORM);
        try{
            message.setContentObject((Serializable)injuredPeople);
        } catch (IOException e){ rcoord.log(e.toString()); assert false;}
        message.clearAllReceiver();
        message.addReceiver(coordinator);
        
        rcoord.send(message);
        
        // update the list of injured people that cannot be saved
        rcoord.getUnableToSavePeople().clear();
        rcoord.getUnableToSavePeople().addAll(injuredPeople);
    }

    private void sendConclusions(List<Pair<AID, Person>> conclusion) {
        
        for (Pair<AID, Person> pair:conclusion){
            
            ACLMessage order = new ACLMessage(ACLMessage.REQUEST);
            order.setProtocol(MessageContent.VOTING);
            try{
                order.setContentObject(pair.getValue());
            } catch (IOException e){rcoord.log(e.toString()); assert false;}
            
            order.clearAllReceiver();
            order.addReceiver(pair.getKey());
            rcoord.send(order);
            
        }
        
    }
}
