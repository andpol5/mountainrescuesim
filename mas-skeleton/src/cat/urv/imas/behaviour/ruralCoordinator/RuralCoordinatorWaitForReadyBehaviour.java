
package cat.urv.imas.behaviour.ruralCoordinator;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.HelicopterCoordinatorAgent;
import cat.urv.imas.agent.RuralCoordinatorAgent;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.behaviour.helicopterCoordinator.HelicopterCoordinatorStartStepBehaviour;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class RuralCoordinatorWaitForReadyBehaviour extends OneShotBehaviour{

    @Override
    public void action() {
        RuralCoordinatorAgent rcoordinator = (RuralCoordinatorAgent)myAgent;
        int agentsCount = 0;

        agentsCount += rcoordinator.getRuralAgents().size();

        List<Person> saved = new ArrayList<>();
        List<Person> pickedUp = new ArrayList<>();
        try {
            // Wait until all agents have made a move
            for (int i = 0; i < agentsCount; i++) {
                MessageTemplate mt = MessageTemplate.MatchPerformative(MessageContent.READY);
                ACLMessage reply = myAgent.blockingReceive(mt);
                String otherInfo = reply.getReplyWith();
                
                
                Object content = reply.getContentObject();
                if(content != null) {
                    List<Person> savedByAgent = (List<Person>) content;
                    
                    if ((otherInfo == null) ? true : otherInfo.equals(MessageContent.SAVED)){
                        saved.addAll(savedByAgent);
                    } else {
                        pickedUp.addAll(savedByAgent);
                    }
                }
                
            }
        } catch (UnreadableException e) {
            e.printStackTrace();
        }
        
        UtilsAgents.sendReady(rcoordinator, AgentType.COORDINATOR, (Serializable) saved, MessageContent.SAVED);
        UtilsAgents.sendReady(rcoordinator, AgentType.COORDINATOR, (Serializable) pickedUp, MessageContent.PICK_UP);
        rcoordinator.addBehaviour(new RuralCoordinatorStartStepBehaviour());
    }
    
}
