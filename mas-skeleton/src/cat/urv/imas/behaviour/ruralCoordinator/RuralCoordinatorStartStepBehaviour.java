
package cat.urv.imas.behaviour.ruralCoordinator;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.ImasAgent;
import cat.urv.imas.agent.RuralCoordinatorAgent;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class RuralCoordinatorStartStepBehaviour extends OneShotBehaviour {
    
    RuralCoordinatorAgent rcoord;

    @Override
    public void action() {
        rcoord = (RuralCoordinatorAgent)myAgent;
        
        // Wait for info
        MessageTemplate mtRequest = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
        ACLMessage reply = myAgent.blockingReceive(mtRequest);
        List<Person> injuredPeople = new ArrayList<>();
        
        // Take the lightly ijured people list from the reply
        try{
            injuredPeople = (List<Person>)reply.getContentObject();
        } catch (UnreadableException e){rcoord.log(e.toString()); assert false;}
        
        // Check if there are any modifications
        if (modifications(injuredPeople)){
            
            // update injured people
            rcoord.getInjuredPeople().clear();
            rcoord.getInjuredPeople().addAll(injuredPeople);
            rcoord.getUnableToSavePeople().clear();
            
            // Announcing rural agents
            for(AID ruralAgent: rcoord.getRuralAgents()) {
                ACLMessage order = new ACLMessage(ACLMessage.REQUEST);
                order.addReceiver(ruralAgent);
                order.setPerformative(MessageContent.START_STEP);
                order.setContent(MessageContent.VOTING);
                rcoord.log("sending voting request to rural agent " + ruralAgent.toString());
                rcoord.send(order);
            }
            
            myAgent.addBehaviour(new RuralCoordinatorVotingBehaviour());
            
        } else {
            
            // Send lightly injured people that cannod be saved by rurals from last step
            // back to coordinator
            // ------------------------------------
            ServiceDescription searchCriterionCoordinator = new ServiceDescription();
            searchCriterionCoordinator.setType(AgentType.COORDINATOR.toString());
            AID coordinator = UtilsAgents.searchAgent(myAgent, searchCriterionCoordinator);

            ACLMessage inform = new ACLMessage(ACLMessage.INFORM);
            inform.addReceiver(coordinator);
            try{
                inform.setContentObject((Serializable)rcoord.getUnableToSavePeople());
            } catch (IOException e){rcoord.log(e.toString()); assert false;}
            myAgent.send(inform);
            // -------------------------------------
            
            // Announcing rural agents
            for(AID ruralAgent: rcoord.getRuralAgents()) {
                ACLMessage order = new ACLMessage(ACLMessage.REQUEST);
                order.addReceiver(ruralAgent);
                order.setPerformative(MessageContent.START_STEP);
                order.setContent(MessageContent.DO_ACTION);
                rcoord.log("sending movement request to rural agent " + ruralAgent.toString());
                rcoord.send(order);
            }
            
            rcoord.addBehaviour(new RuralCoordinatorWaitForReadyBehaviour());
        }
    }

    private boolean modifications(List<Person> injuredPeople) {
        
        // Check if there are changes in injured people
        boolean injuredPeopleChanges = false;
        if (rcoord.getInjuredPeople().size() != injuredPeople.size()) 
            injuredPeopleChanges = true;
        if (!injuredPeopleChanges){
            for (Person p:injuredPeople){
                injuredPeopleChanges = true;
                for (Person o: rcoord.getInjuredPeople()){
                    if (o.equals(p)){
                        injuredPeopleChanges = false;
                        break;
                    }
                }
                if (injuredPeopleChanges){
                    break;
                }
            }
        }
        
        // If there are changes in avalanches
        boolean avalancheChanges = rcoord.avalancheModifications();
        
        if (injuredPeopleChanges) return true;
        if (avalancheChanges){
            if (injuredPeople.size() > 0){
                return true;
            }
        }
        
        return false;
        
    }
    
}
