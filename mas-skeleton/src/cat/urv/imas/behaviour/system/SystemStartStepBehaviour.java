
package cat.urv.imas.behaviour.system;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.SystemAgent;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.CellType;
import cat.urv.imas.map.PathCell;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import java.io.IOException;
import java.util.ArrayList;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class SystemStartStepBehaviour extends OneShotBehaviour {

    SystemAgent system;

    @Override
    public void action() {
        system = (SystemAgent) getAgent();
        // Increment step counter
        system.getGame().getGameMap().nextStep();

        // UPDATING GUI
        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int steps = system.getGame().getSimulationSteps();
        system.updateGUI();
        int currentStep = system.getGame().getGameMap().getStep();
        if (currentStep > steps) {
            system.log("!! simulation FINISHED !!");
            return;
        }
        system.log("step " + currentStep + " finished; gui updated");

        // MAKING CHANGES TO THE ENVIRONMENT
        changeEnvironment();

        // SENDING INFO
        sendInfo();

        // WAITING FOR THE END OF THE STEP
        system.addBehaviour(new SystemWaitForReadyBehaviour());

    }
    
    private void changeEnvironment(){
        Random random = new Random();
        
        // getting path cells
        List<Cell> pathcells = system.getGame().getGameMap().getCells(CellType.PATH);
        
        // Clearing dead people and avalanches
        int numberOfDead = 0;
        for (Cell c: pathcells){
            PathCell pc = (PathCell)c;
            pc.cleanAvalanche(system.getGame().getGameMap().getStep());
            numberOfDead += pc.getInjuredPeople().cleanDiedPeople(system.getGame().getGameMap().getStep());
        }
        system.getGame().cleanDeadpeople(); //no need to count this, the same people counted above
        
        // injured people?
        if (random.nextInt(100) < system.getGame().getInjuredChance()){
            
            // How many injured people?
            int peopleNumber = UtilsAgents.randomDescendingDistribution(system.getGame().getMaximumPeoplePerStep());
            for (; peopleNumber > 0;peopleNumber--){
                int maxIndex = pathcells.size() - 1;
                int index = random.nextInt(maxIndex);
                PathCell toChange = (PathCell)pathcells.get(index);
                int stepToDie = system.getGame().getGameMap().getStep() + system.getGame().getStepsToFreeze();
                toChange.addInjuredPeople(1, stepToDie);
                
                // lightly injured?
                boolean light = false;
                if (random.nextInt(100) < system.getGame().getLightSeverity()){
                    light = true;
                }
                system.getGame().getCurrentInjured().add(new Person(toChange.getRow(),toChange.getCol(),light,stepToDie));
            }
        }
        
        
        // avalanches?
        if (random.nextInt(100) < system.getGame().getAvalancheChance()){
            // How many avalanches?
            int avalancheNumber = UtilsAgents.randomDescendingDistribution(system.getGame().getMaximumAvalanchesPerStep());
            for (; avalancheNumber > 0;avalancheNumber--){
                int maxIndex = pathcells.size() - 1;
                int index = random.nextInt(maxIndex);
                PathCell toChange = (PathCell)pathcells.get(index);
                
                int stepToDie = system.getGame().getGameMap().getStep() + system.getGame().getAvalancheDuration();
                // If avalanches kill injured persons
                numberOfDead += toChange.setAvalanche(stepToDie);
            }
        }
        // NOTE 1: there can be more avalanches appearing on the same path cell, but only one will remain,
        //         thus further reducing the chance for a maximum number of avalanches per step

        system.getStatistics().addDiedPeople(numberOfDead);
    }
        
    private void sendInfo(){
        // Send current injured people list to coordinator
        ServiceDescription searchCriterionCoordinator = new ServiceDescription();
        searchCriterionCoordinator.setType(AgentType.COORDINATOR.toString());
        AID coordinator = UtilsAgents.searchAgent(myAgent, searchCriterionCoordinator);
        
        ACLMessage order = new ACLMessage(ACLMessage.REQUEST);
        order.addReceiver(coordinator);
        try{
            order.setContentObject((ArrayList)(system.getGame().getCurrentInjured()));
        } catch (IOException e) {system.log(e.toString()); assert false;}
        system.log("sending injured people list to " + coordinator.toString());
        myAgent.send(order);
    }

}
