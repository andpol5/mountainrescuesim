
package cat.urv.imas.behaviour.system;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.ImasAgent;
import cat.urv.imas.agent.SystemAgent;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.behaviour.coordinator.CoordinatorWaitForReadyBehaviour;
import cat.urv.imas.gui.Statistics;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.CellType;
import cat.urv.imas.map.PathCell;
import cat.urv.imas.onthology.InfoAgent;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.util.*;


public class SystemWaitForReadyBehaviour extends OneShotBehaviour {

    @Override
    public void action() {
        SystemAgent system = (SystemAgent) myAgent;

        // Waiting for coordinator to send ready
        MessageTemplate mtrs = MessageTemplate.and(MessageTemplate.MatchPerformative(MessageContent.READY),
                                                  MessageTemplate.MatchReplyWith(MessageContent.SAVED));
        ACLMessage savedReadyMsg = myAgent.blockingReceive(mtrs);
        
        MessageTemplate mtrp = MessageTemplate.and(MessageTemplate.MatchPerformative(MessageContent.READY),
                                                  MessageTemplate.MatchReplyWith(MessageContent.PICK_UP));
        ACLMessage pickedUpReadyMsg = myAgent.blockingReceive(mtrp);
        

        try {
            // ready message should provide people saved
            Object content = savedReadyMsg.getContentObject();
            if (content != null) {
                Map<AgentType, List<Person>> saved = (Map<AgentType, List<Person>>) content;
                markRescued(saved);
            }
            // ready message should also provide people pickedup
            Object content2 = pickedUpReadyMsg.getContentObject();
            if (content2 != null) {
                List<Person> pickedUp = (List<Person>) content2;
                // mark picked up
                for (Person p: pickedUp)
                    system.getGame().removeInjuredPerson(p);
            }


        } catch (UnreadableException e) {
            e.printStackTrace();
        }

        system.addBehaviour(new SystemStartStepBehaviour());
    }

    private void markRescued(Map<AgentType, List<Person>> saved) {
        SystemAgent system = (SystemAgent) myAgent;
        Statistics statistics = system.getStatistics();

        List<Person> byHelicopters = saved.get(AgentType.HELICOPTER);
        List<Person> byRurals = saved.get(AgentType.RURAL_AGENT);
        if ((byRurals == null || byRurals.isEmpty()) && (byHelicopters == null || byHelicopters.isEmpty())) {
            return;
        }

        List<Person> all = new ArrayList<>();
        if (byHelicopters != null) {
            all.addAll(byHelicopters);
            statistics.addHospitalRescue(byHelicopters.size());

        }
        if (byRurals != null) {
            all.addAll(byRurals);
            statistics.addRuralAgentRescue(byRurals.size());
        }

        int stepsToFreeze = system.getGame().getStepsToFreeze();

        for (Person person : all) {
            if (person != null){
                int stepsToResque = stepsToFreeze - (person.getStepToDie() - system.getGame().getGameMap().getStep());
                statistics.addFirstVisit(stepsToResque);
            }
        }

    }
}
