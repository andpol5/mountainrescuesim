package cat.urv.imas.behaviour.moveable;


import cat.urv.imas.agent.MoveableAgent;
import cat.urv.imas.map.Cell;
import jade.core.behaviours.OneShotBehaviour;

import java.util.LinkedList;
import java.util.List;

public class MakeMoveBehaviour extends OneShotBehaviour {

    protected MoveableAgent agent;
    protected List<Cell> surroundingCells;

    @Override
    public void action() {
        this.agent = (MoveableAgent)myAgent;
    }
    protected List<Cell> retrieveSurroundingCells(Cell cell) {
        Cell[][] map = agent.getGameMap().getMap();
        int row = cell.getRow();
        int col = cell.getCol();

        surroundingCells = new LinkedList<>();
        for(int i = -1; i<2; i++ ){
            if (row+i >= 0 && row + i < map.length){
                for(int j = -1; j<2;j++){
                    if (col+j >= 0 && col + j < map[0].length){
                        surroundingCells.add(map[row+i][col+j]);
                    }
                }
            }

        }

        return surroundingCells;
    }
}
