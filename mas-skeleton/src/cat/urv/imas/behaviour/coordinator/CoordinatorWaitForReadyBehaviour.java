package cat.urv.imas.behaviour.coordinator;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.ImasAgent;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class CoordinatorWaitForReadyBehaviour extends OneShotBehaviour {


    @Override
    public void action() {
        
        // Search helicopter coordinator
        ServiceDescription searchCriterionHelicopterCoordinator = new ServiceDescription();
        searchCriterionHelicopterCoordinator.setType(AgentType.HELICOPTER_COORDINATOR.toString());
        AID helicopterCoordinator = UtilsAgents.searchAgent(myAgent, searchCriterionHelicopterCoordinator);
        
        // Search rural coordinator
        ServiceDescription searchCriterionRuralCoordinator = new ServiceDescription();
        searchCriterionRuralCoordinator.setType(AgentType.RURAL_AGENT_COORDINATOR.toString());
        AID ruralCoordinator = UtilsAgents.searchAgent(myAgent, searchCriterionRuralCoordinator);
        

        try {
            // Wait for both coordinators to send ready (saved people and picked up people so 4 messages in total)
            MessageTemplate mtrural = MessageTemplate.and(MessageTemplate.MatchPerformative(MessageContent.READY), MessageTemplate.MatchSender(ruralCoordinator));
            ACLMessage savedByRuralMessage = myAgent.blockingReceive(mtrural);
            ACLMessage pickedUpByRuralMessage = myAgent.blockingReceive(mtrural);

            Object savedByRural = savedByRuralMessage.getContentObject();
            Object pickedUpByRural = pickedUpByRuralMessage.getContentObject();

            MessageTemplate mthelicopter = MessageTemplate.and(MessageTemplate.MatchPerformative(MessageContent.READY), MessageTemplate.MatchSender(helicopterCoordinator));
            ACLMessage savedByHelicopterMessage = myAgent.blockingReceive(mthelicopter);
            ACLMessage pickedUpByHelicopterMessage = myAgent.blockingReceive(mthelicopter);
            
            Object savedByHelicopter = savedByHelicopterMessage.getContentObject();
            Object pickedUpByHelicopter = pickedUpByHelicopterMessage.getContentObject();

            Map<AgentType, List<Person>> saved = null;
            List<Person> pickedUp = new ArrayList<>();

            if (savedByHelicopter != null || savedByRural!=null) {
                List<Person> savedByRurals = (List<Person>) savedByRural;
                List<Person> savedByHelicopters = (List<Person>) savedByHelicopter;
                saved = new HashMap<>();
                saved.put(AgentType.HELICOPTER, savedByHelicopters);
                saved.put(AgentType.RURAL_AGENT, savedByRurals);
            }
            if (pickedUpByHelicopter != null || pickedUpByRural !=null) {
                List<Person> pickedUpByRurals = (List<Person>) pickedUpByRural;
                List<Person> pickedUpByHelicopters = (List<Person>) pickedUpByHelicopter;
                if (pickedUpByHelicopters != null)
                    pickedUp.addAll(pickedUpByHelicopters);
                if (pickedUpByRurals != null)
                    pickedUp.addAll(pickedUpByRurals);
            }
            UtilsAgents.sendReady((ImasAgent)myAgent, AgentType.SYSTEM, (Serializable) saved, MessageContent.SAVED);
            UtilsAgents.sendReady((ImasAgent)myAgent, AgentType.SYSTEM, (Serializable) pickedUp, MessageContent.PICK_UP);
            myAgent.addBehaviour(new CoordinatorStartStepBehaviour());
        } catch (UnreadableException e) {
            e.printStackTrace();
            assert false;
        }



    }
}
