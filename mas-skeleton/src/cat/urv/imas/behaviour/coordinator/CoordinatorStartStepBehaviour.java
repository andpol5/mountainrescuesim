/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.coordinator;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.CoordinatorAgent;
import cat.urv.imas.agent.MoveableAgent;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CoordinatorStartStepBehaviour extends OneShotBehaviour {

    @Override
    public void action() {
        CoordinatorAgent coord = (CoordinatorAgent)myAgent;
        
        // Wait for order from system
        MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
        ACLMessage reply = myAgent.blockingReceive(mt);
        
        // Take the ijured people list from the reply
        try{
            
            List<Person> injuredPeople = (List<Person>)reply.getContentObject();
            passInfo(injuredPeople);

        } catch (UnreadableException e){coord.log(e.toString()); assert false;}
                
        // now waiting for moves to finish
        myAgent.addBehaviour(new CoordinatorWaitForReadyBehaviour());
    }

    private void passInfo(List<Person> injuredPeople) {
        CoordinatorAgent coord = (CoordinatorAgent)myAgent;
        // Splitting the list in 2 lists with lightly injured and severly injured
        List<Person> lightlyInjured = new ArrayList<Person>();
        for(Person p:injuredPeople){
            if (p.getLight()){
                lightlyInjured.add(p);
            }
        }
        for (Person p: lightlyInjured){
            injuredPeople.remove(p);
        }
        
        // Search helicopter coordinator
        ServiceDescription searchCriterionHelicopterCoordinator = new ServiceDescription();
        searchCriterionHelicopterCoordinator.setType(AgentType.HELICOPTER_COORDINATOR.toString());
        AID helicopterCoordinator = UtilsAgents.searchAgent(myAgent, searchCriterionHelicopterCoordinator);
        
        // Search rural coordinator
        ServiceDescription searchCriterionRuralCoordinator = new ServiceDescription();
        searchCriterionRuralCoordinator.setType(AgentType.RURAL_AGENT_COORDINATOR.toString());
        AID ruralCoordinator = UtilsAgents.searchAgent(myAgent, searchCriterionRuralCoordinator);
        
        // Send severly injured to helicopter coordinator
        ACLMessage order = new ACLMessage(ACLMessage.REQUEST);
        order.addReceiver(helicopterCoordinator);
        try{
            order.setContentObject((ArrayList)injuredPeople);
        } catch (IOException e){coord.log(e.toString()); assert false;}
        myAgent.send(order);
        
        // Send lightly injured to rural coordinator
        order = new ACLMessage(ACLMessage.REQUEST);
        order.addReceiver(ruralCoordinator);
        try{
        order.setContentObject((ArrayList)lightlyInjured);
        } catch (IOException e){coord.log(e.toString()); assert false;}
        myAgent.send(order); 

        // Wait for the lightly injured people the rurals cannot save
        MessageTemplate mtrural = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),MessageTemplate.MatchSender(ruralCoordinator));
        ACLMessage reply = myAgent.blockingReceive(mtrural);
        try{
            lightlyInjured = (List<Person>)reply.getContentObject();
        } catch (UnreadableException e){coord.log(e.toString()); assert false;}
        
        // Sending the lightly injured people that cannot be saved by rurals to helicopter coordinator
        order = new ACLMessage(ACLMessage.REQUEST);
        order.addReceiver(helicopterCoordinator);
        try{
            order.setContentObject((ArrayList)lightlyInjured);
        } catch (IOException e){coord.log(e.toString()); assert false;}
        myAgent.send(order); 
             
    }
    
}
