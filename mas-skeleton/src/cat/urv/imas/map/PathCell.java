/**
 * IMAS base code for the practical work.
 * Copyright (C) 2014 DEIM - URV
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.map;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.gui.CellVisualizer;
import cat.urv.imas.onthology.InfoAgent;
import jade.core.AID;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This class keeps information about a street cell in the map.
 */
public class PathCell extends Cell {

    /**
     * Injures people list.
     */
    private InjuredPeople injuredPeople = new InjuredPeople();
    /**
     * If greater than 0, this path has an avalanche, and is bound to end on the step
     * equal to this variable. 0 means that the path is free and available.
     */
    private int avalanche = 0;

    /**
     * Builds a cell with a given type.
     *
     * @param row row number.
     * @param col column number.
     */
    public PathCell(int row, int col) {
        super(CellType.PATH, row, col);
    }
    
    public PathCell(){
        super();
    }

    public void addInjuredPeople(int amount, int stepToDie) {
        injuredPeople.addInjuredPeople(amount, stepToDie);
    }

    public void removeInjuredPeople(int stepToDie) {
        injuredPeople.removeInjuredPerson(stepToDie);
    }

    public InjuredPeople getInjuredPeople() {
        return injuredPeople;
    }

    /**
     * Is this path cell covered by an avalanche?
     * @return has an avalanche.
     */
    public boolean isAvalanche() {
        return avalanche>0;
    }
    
    /**
     * Cleans this path cell if the avalanche has reached its last step.
     */
    public void cleanAvalanche(int currentStep){
        if (avalanche <= currentStep){
            avalanche = 0;
        }
    }

    /**
     * Sets or un-sets whether this cell is an avalanche.
     * @param avalanche >0 to contain an avalanche. 0 otherwise.
     * @return number of dead people if th avalanche kills the injured people
     */
    public int setAvalanche(int avalanche) {
        int numberOfDead = 0;

        this.avalanche = avalanche;
        return numberOfDead;
    }

    /* ***************** Map visualization API ********************************/
    @Override
    public void draw(CellVisualizer visual) {
        if (isAvalanche()) {
            visual.drawAvalanche(this);
        } else {
            visual.drawPath(this);
        }
    }
    
    @Override
    public String getMapMessage() {
        StringBuffer sb = new StringBuffer();
        String newline = System.getProperty("line.separator");
        if (this.isThereAnAgent()) {
            for (AgentType at : agents.keySet()) {
                if (agents.get(at).size() > 0) {
                    sb.append(at.getValue());
                    sb.append(":");
                    sb.append(agents.get(at).size());
                    sb.append(newline);
                }
            }
        }
        if (injuredPeople.isThereInjuredPeople()){
            sb.append("P:");
            sb.append(injuredPeople.howManyInjured());
            sb.append(newline);
        }
        if(sb.length() < 1){
            return "";
        }
        return sb.toString();
    }
}
