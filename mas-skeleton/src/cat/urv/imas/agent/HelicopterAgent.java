/**
 * IMAS code for the practical work.
 * Copyright (C) 2014 DEIM - URV
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.behaviour.helicopter.HelicopterContractNetResponder;
import cat.urv.imas.behaviour.helicopter.HelicopterStartStepBehaviour;
import cat.urv.imas.map.Cell;
import cat.urv.imas.onthology.HelicopterStartDescription;
import cat.urv.imas.onthology.MapArea;
import cat.urv.imas.onthology.Person;
import cat.urv.imas.onthology.Task;
import cat.urv.imas.onthology.TaskType;
import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.FIPANames.InteractionProtocol;
import jade.lang.acl.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * The helicopter agent is a simulated helicopter with the ability to cross mountains.
 */
public class HelicopterAgent extends MoveableAgent{
    
    /**
     * Every Helicopter agent can have up to 2 tasks
     */
    private Task task1;
    private Task task2;
    private boolean readyForCN;
    
    /**
     *  the list that contains the saved persons this turn.
     */
    private List<Person> saved;
    
    private List<Person> pickedUp;
    
    /**
     * The map area the helicopter is bound to
     */
    private MapArea area;
    
    /*
    The path
    */
    private Queue<Cell> path;
    
    /**
     * Builds the Helicopter agent.
     */
    public HelicopterAgent() {
        super(AgentType.HELICOPTER);
    }

    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {
        super.setup();
        log("I am helicopter, my cell is: (" + myCell.getRow() + ", " + myCell.getCol() + ")");

        /* ** Very Important Line (VIL) ************************************* */
        this.setEnabledO2ACommunication(true, 1);
        /* ********************************************************************/
        
        // Initializing the tasks
        task1 = new Task();
        task2 = new Task();
        readyForCN = false;
        
        // Register the agent to the DF
        ServiceDescription sd1 = new ServiceDescription();
        sd1.setType(AgentType.HELICOPTER.toString());
        sd1.setName(getLocalName());
        sd1.setOwnership(OWNER);
        
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.addServices(sd1);
        dfd.setName(getAID());
        try {
            DFService.register(this, dfd);
            log("Registered to the DF");
        } catch (FIPAException e) {
            System.err.println(getLocalName() + " registration with DF failed. Reason: " + e.getMessage());
            doDelete();
        }
        
         saved = new ArrayList<>();
         path = new LinkedList<>();
         pickedUp = new ArrayList<>();
         
        UtilsAgents.sendReady(this, AgentType.HELICOPTER_COORDINATOR, 
                              new HelicopterStartDescription(myCell.getRow(), myCell.getCol(),this.getAID()));
        
        // Waiting for the map area
        MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
        ACLMessage reply = blockingReceive(mt);
        
        // Retrieving the map area
        try{
            area = (MapArea)reply.getContentObject();
        } catch (UnreadableException e) { log(e.toString()); assert false;}
        
        MessageTemplate cfpt = MessageTemplate.and(
                   MessageTemplate.MatchPerformative(ACLMessage.CFP),
                   MessageTemplate.and(
                        MessageTemplate.MatchProtocol(InteractionProtocol.FIPA_CONTRACT_NET),
                        MessageTemplate.MatchOntology(area.getId())
                        )
                   );
        addBehaviour(new HelicopterContractNetResponder(this, cfpt));
        
        addBehaviour(new HelicopterStartStepBehaviour());
    }

    /**
     * @return the task1
     */
    public Task getTask1() {
        return task1;
    }

    /**
     * @return the task2
     */
    public Task getTask2() {
        return task2;
    }

    /**
     * @return the area
     */
    public MapArea getArea() {
        return area;
    }

    /**
     * @return the path
     */
    public Queue<Cell> getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(Queue<Cell> path) {
        this.path = path;
    }

    /**
     * @return the saved
     */
    public List<Person> getSaved() {
        return saved;
    }
    
    /**
     * If the helicopter has 2 tasks (save x or x to hospital) then returns true.
     */
    public boolean isBusy(){
        
        boolean isTask1 = (task1.getType() == TaskType.SAVE_X) || (task1.getType() == TaskType.X_TO_SAFETY);
        boolean isTask2 = (task2.getType() == TaskType.SAVE_X) || (task2.getType() == TaskType.X_TO_SAFETY);
        
        return isTask1 && isTask2;
    }

    /**
     * @return the readyForCN
     */
    public boolean isReadyForCN() {
        return readyForCN;
    }

    /**
     * @param readyForCN the readyForCN to set
     */
    public void setReadyForCN(boolean readyForCN) {
        this.readyForCN = readyForCN;
    }

    /**
     * @return the pickedUp
     */
    public List<Person> getPickedUp() {
        return pickedUp;
    }

    /**
     * @param pickedUp the pickedUp to set
     */
    public void setPickedUp(List<Person> pickedUp) {
        this.pickedUp = pickedUp;
    }

}
