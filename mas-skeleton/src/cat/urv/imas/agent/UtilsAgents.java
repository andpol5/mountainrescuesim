/**
 * IMAS base code for the practical work.
 * Copyright (C) 2014 DEIM - URV
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.map.Cell;
import cat.urv.imas.map.CellType;
import cat.urv.imas.onthology.MessageContent;
import jade.core.Agent;
import jade.core.AID;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Random;

/**
 * Utility class for agents.
 */
public class UtilsAgents {

    /**
     * After each search of an agent, we will wait for 2 seconds before retrying.
     */
    private static final long DELAY = 2000;

    /**
     * To prevent being instanced.
     */
    private UtilsAgents() {
    }


    /**
     * To search an agent of a certain type
     *
     * @param parent Agent
     * @param sd ServiceDescription search criterion
     * @return AID of the agent if it is foun, it is a *blocking* method
     */
    public static AID searchAgent(Agent parent, ServiceDescription sd) {
        /**
         * Searching an agent of the specified type
         */
        AID searchedAgent = new AID();
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.addServices(sd);
        try {
            while (true) {
                SearchConstraints c = new SearchConstraints();
                c.setMaxResults(new Long(-1));
                DFAgentDescription[] result = DFService.search(parent, dfd, c);
                if (result.length > 0) {
                    dfd = result[0];
                    searchedAgent = dfd.getName();
                    break;
                }
                Thread.sleep(DELAY);

            }
        } catch (Exception fe) {
            System.err.println("ERROR: Cannot search the expected agent from parent " + parent.getLocalName());
            fe.printStackTrace();
            parent.doDelete();
        }
        return searchedAgent;
    }

    /**
     * To search an agent of a certain type
     *
     * @param parent Agent
     * @param sd ServiceDescription search criterion
     * @return AID of the agent if it is foun, it is a *blocking* method
     */
    public static List<AID> searchAgents(Agent parent, ServiceDescription sd) {
        /**
         * Searching an agent of the specified type
         */
        List<AID> searchedAgents = new ArrayList();
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.addServices(sd);
        try {
            SearchConstraints c = new SearchConstraints();
            c.setMaxResults(new Long(-1));
            DFAgentDescription[] result = DFService.search(parent, dfd, c);
            for (DFAgentDescription res : result) {
                searchedAgents.add(res.getName());
            }
        } catch (Exception fe) {
            System.err.println("ERROR: Cannot search the expected agents from parent " + parent.getLocalName());
            fe.printStackTrace();
            parent.doDelete();
        }
        return searchedAgents;
    }

    /**
     * To create an agent in a given container
     *
     * @param container AgentContainer
     * @param agentName String Agent name
     * @param className String Agent class
     * @param arguments Object[] Arguments; null, if they are not needed
     */
    public static AgentController createAgent(AgentContainer container, String agentName, String className, Object[] arguments) {
        try {
            AgentController controller = container.createNewAgent(agentName, className, arguments);
            controller.start();
            return controller;
        } catch (StaleProxyException e) {
            System.err.println("ERROR: Cannot create agent " + agentName + " of class " + className);
            e.printStackTrace();
        }
        return null;
    }

    /**
     * To create the agent and the container together. You can specify a
     * container and reuse it.
     *
     * @param agentName String Agent name
     * @param className String Class
     * @param arguments Object[] Arguments
     */
    public static void createAgent(String agentName, String className, Object[] arguments) {
        try {
            Runtime rt = Runtime.instance();
            Profile p = new ProfileImpl();
            AgentContainer container = rt.createAgentContainer(p);

            AgentController controller = container.createNewAgent(agentName, className, arguments);
            controller.start();
        } catch (Exception e) {
            System.err.println("ERROR: Cannot create agent " + agentName + " of class " + className);
            e.printStackTrace();
        }
    }

    /**
     * To create the agent and the container together, returning the container.
     *
     * @param agentName String Agent name
     * @param className String Class
     * @param arguments Object[] Arguments
     * @return AgentContainer created
     */
    public static AgentContainer createAgentGetContainer(String agentName, String className, Object[] arguments) {
        AgentContainer container = null;
        try {
            Runtime rt = Runtime.instance();
            Profile p = new ProfileImpl();
            container = rt.createAgentContainer(p);

            AgentController controller = container.createNewAgent(agentName, className, arguments);
            controller.start();
        } catch (Exception e) {
            System.err.println("ERROR: Cannot create agent " + agentName + " of class " + className);
            e.printStackTrace();
        }
        return container;
    }
    
    public static void sendReady(ImasAgent from, AgentType toType, Serializable object){
        // search for agent by type (since alway sending to a higher level, there is only one 
        // agent of that type
        ServiceDescription searchCriterion = new ServiceDescription();
        searchCriterion.setType(toType.toString());
        AID toAgent = UtilsAgents.searchAgent(from, searchCriterion);
        
        ACLMessage initialRequest = new ACLMessage(ACLMessage.INFORM);
        initialRequest.clearAllReceiver();
        initialRequest.addReceiver(toAgent);
        from.log("Ready message to " + toType.toString() + ".");
        try {
            initialRequest.setPerformative(MessageContent.READY);
            if(object != null) {
                initialRequest.setContentObject(object);
            }
            from.send(initialRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
        public static void sendReady(ImasAgent from, AgentType toType, Serializable object, String otherinfo){
        // search for agent by type (since alway sending to a higher level, there is only one 
        // agent of that type
        ServiceDescription searchCriterion = new ServiceDescription();
        searchCriterion.setType(toType.toString());
        AID toAgent = UtilsAgents.searchAgent(from, searchCriterion);
        
        ACLMessage initialRequest = new ACLMessage(ACLMessage.INFORM);
        initialRequest.clearAllReceiver();
        initialRequest.addReceiver(toAgent);
        from.log("Ready message to " + toType.toString() + ".");
        try {
            initialRequest.setPerformative(MessageContent.READY);
            if(object != null) {
                initialRequest.setContentObject(object);
            }
            initialRequest.setReplyWith(otherinfo);
            from.send(initialRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * For system to decide some random numbers when changing the environment while using 
     * liniar descending distribution (the first is the more likely, then the next,
     * then the next)
     */
    public static int randomDescendingDistribution(int maxSize){
        //Get a linearly multiplied random number
        int randomMultiplier = maxSize * (maxSize + 1) / 2;
        Random r=new Random();
        int randomInt = r.nextInt(randomMultiplier);

        //Linearly iterate through the possible values to find the correct one
        int linearRandomNumber = 0;
        for(int i=maxSize; randomInt >= 0; i--){
            randomInt -= i;
            linearRandomNumber++;
        }

        return linearRandomNumber;
    }
}
