/**
 * IMAS code for the practical work.
 * Copyright (C) 2014 DEIM - URV
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.behaviour.ruralCoordinator.RuralCoordinatorWaitForReadyBehaviour;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.CellType;
import cat.urv.imas.map.PathCell;
import cat.urv.imas.onthology.GameMap;
import cat.urv.imas.onthology.Person;
import jade.core.*;
import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.FIPANames.InteractionProtocol;
import jade.lang.acl.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Helicopter Coordinator agent is in charge of controlling Helicopter Agents.
 */
public class RuralCoordinatorAgent extends ImasAgent {

    GameMap map;
    private List<PathCell> avalanches;
    private List<Person> unableToSavePeople;
    private List<Person> injuredPeople;
    
    /**
     * Builds the Rural coordinator agent.
     */
    public RuralCoordinatorAgent() {
        super(AgentType.RURAL_AGENT_COORDINATOR);
    }

    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {

        /* ** Very Important Line (VIL) ************************************* */
        this.setEnabledO2ACommunication(true, 1);
        /* ********************************************************************/
        
        Object[] args = getArguments();
        map = (GameMap)args[0];
        avalanches = getAvalanches();
        
        unableToSavePeople = new ArrayList<>();
        injuredPeople = new ArrayList<>();
        
        // Register the agent to the DF
        ServiceDescription sd1 = new ServiceDescription();
        sd1.setType(AgentType.RURAL_AGENT_COORDINATOR.toString());
        sd1.setName(getLocalName());
        sd1.setOwnership(OWNER);
        
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.addServices(sd1);
        dfd.setName(getAID());
        try {
            DFService.register(this, dfd);
            log("Registered to the DF");
        } catch (FIPAException e) {
            System.err.println(getLocalName() + " registration with DF failed. Reason: " + e.getMessage());
            doDelete();
        }

        
        log("created a RuralCoordinatorAgent");
        addBehaviour(new RuralCoordinatorWaitForReadyBehaviour());
    }
    
    public GameMap getMap() {
        return map;
    }
    
        private List<PathCell> getAvalanches() {
        List<PathCell> newAvalanches = new ArrayList<>();
        
        List<Cell> pathCells = map.getCells(CellType.PATH);
        for (Cell pathcell: pathCells){
            PathCell p = (PathCell) pathcell;
            if (p.isAvalanche()){
                newAvalanches.add(p);
            }
        }
        
        return newAvalanches;
    }
        
    public boolean avalancheModifications(){
        List<PathCell> newAvalanches = getAvalanches();
        
        if (avalanches.size() != newAvalanches.size())
            return true;
        
        boolean modification = false;
        for (PathCell newAvalanche: newAvalanches){
            modification = true;
            for (PathCell avalanche: avalanches){
                boolean samerow = avalanche.getRow() == newAvalanche.getRow();
                boolean samecol = avalanche.getCol() == newAvalanche.getCol();
                if (samerow && samecol){
                    modification = false;
                    break;
                }
            }
            if (modification) break;
        }
        
        return modification;
    }

    /**
     * @return the unableToSavePeople
     */
    public List<Person> getUnableToSavePeople() {
        return unableToSavePeople;
    }

    /**
     * @param unableToSavePeople the unableToSavePeople to set
     */
    public void setUnableToSavePeople(List<Person> unableToSavePeople) {
        this.unableToSavePeople = unableToSavePeople;
    }

    /**
     * @return the injuredPeople
     */
    public List<Person> getInjuredPeople() {
        return injuredPeople;
    }

    /**
     * @param injuredPeople the injuredPeople to set
     */
    public void setInjuredPeople(List<Person> injuredPeople) {
        this.injuredPeople = injuredPeople;
    }
    
    public List<AID> getRuralAgents(){
        ServiceDescription searchCriterion = new ServiceDescription();
        searchCriterion.setType(AgentType.RURAL_AGENT.toString());
        List<AID> rurals = UtilsAgents.searchAgents(this, searchCriterion);
        return rurals;
    }

}
