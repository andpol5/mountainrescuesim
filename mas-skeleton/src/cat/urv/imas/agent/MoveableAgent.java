/**
 * IMAS code for the practical work.
 * Copyright (C) 2014 DEIM - URV
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.map.Cell;
import cat.urv.imas.onthology.GameMap;
import cat.urv.imas.onthology.MessageContent;
import jade.core.*;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.FIPANames.InteractionProtocol;
import jade.lang.acl.*;

/**
 * The rural agent is a simulated mountaineer who can rescue injured people on foot.
 */
public class MoveableAgent extends ImasAgent {

    public void setMyCell(Cell myCell) {
        this.myCell = myCell;
    }

    Cell myCell;
    GameMap map;

    public Cell getMyCell(){
        return myCell;
    }

    public GameMap getGameMap(){
        return map;
    }
    
    /**
     * Creates the agent.
     * @param type type of agent to set.
     */
    public MoveableAgent(AgentType type) {
        super(type);
    }

    @Override
    protected void setup() {
        Object[] args = getArguments();
        myCell = (Cell)args[0];
        map = (GameMap)args[1];
    }
    
    public void sendFinishMovementMessage(){
        // search for SystemAgent
        ServiceDescription searchCriterion = new ServiceDescription();
        searchCriterion.setType(AgentType.SYSTEM.toString());
        AID systemAgent = UtilsAgents.searchAgent(this, searchCriterion);
        
        ACLMessage initialRequest = new ACLMessage(ACLMessage.INFORM);
        initialRequest.clearAllReceiver();
        initialRequest.addReceiver(systemAgent);
        initialRequest.setProtocol(InteractionProtocol.FIPA_CONTRACT_NET);
        log("Finish movement message to sysAgent");
        try {
            initialRequest.setPerformative(MessageContent.FINISH_MOVEMENT);
            send(initialRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void moveToOtherCell(Cell desiredCell) throws Exception {
        
        map.removeAgent(type, myCell, getAID());
        map.addAgentToMap(type, desiredCell, getAID());
        
        myCell = desiredCell;
    }

}
