/**
 * IMAS code for the practical work.
 * Copyright (C) 2014 DEIM - URV
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.behaviour.rural.RuralStartStepBehaviour;
import cat.urv.imas.map.Cell;
import cat.urv.imas.onthology.Person;
import cat.urv.imas.onthology.Task;
import cat.urv.imas.onthology.TaskType;
import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * The rural agent is a simulated mountaineer who can rescue injured people on foot.
 * TODO: everything
 */
public class RuralAgent extends MoveableAgent {
    private Task task;
    private Queue<Cell> path;
    private List<Person> saved;
    private List<Person> pickedUp;
    
    /**
     * Builds the Rural agent.
     */
    public RuralAgent() {
        super(AgentType.RURAL_AGENT);
    }

    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {
        super.setup();
        log("I am rural, my cell is: (" + myCell.getRow() + ", " + myCell.getCol() + ")");
        /* ** Very Important Line (VIL) ************************************* */
        this.setEnabledO2ACommunication(true, 1);
        /* ********************************************************************/
        
        task = new Task();
        path = new LinkedList<>();
        saved = new ArrayList<>();
        pickedUp = new ArrayList<>();
        
        // Register the agent to the DF
        ServiceDescription sd1 = new ServiceDescription();
        sd1.setType(AgentType.RURAL_AGENT.toString());
        sd1.setName(getLocalName());
        sd1.setOwnership(OWNER);
        
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.addServices(sd1);
        dfd.setName(getAID());
        try {
            DFService.register(this, dfd);
            log("Registered to the DF");
        } catch (FIPAException e) {
            System.err.println(getLocalName() + " registration with DF failed. Reason: " + e.getMessage());
            doDelete();
        }

        UtilsAgents.sendReady(this, AgentType.RURAL_AGENT_COORDINATOR, null);
        addBehaviour(new RuralStartStepBehaviour());
        
    }

    /**
     * @return the task
     */
    public Task getTask() {
        return task;
    }

    /**
     * @return the path
     */
    public Queue<Cell> getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(Queue<Cell> path) {
        this.path = path;
    }

    /**
     * @return the saved
     */
    public List<Person> getSaved() {
        return saved;
    }

    /**
     * @return the pickedUp
     */
    public List<Person> getPickedUp() {
        return pickedUp;
    }

    /**
     * @param pickedUp the pickedUp to set
     */
    public void setPickedUp(List<Person> pickedUp) {
        this.pickedUp = pickedUp;
    }

    public boolean isBusy() {
        if(task.getType() == TaskType.X_TO_SAFETY)
            return true;
       return false;
    }

   
}
