package cat.urv.imas.agent;


import cat.urv.imas.map.Cell;
import cat.urv.imas.onthology.GameSettings;
import jade.core.AID;

public interface IAgentCreator {

    public void createRuralAgent(Cell cell, int i, GameSettings settings);
    public void createHelicopterAgent(Cell cell, int i, GameSettings settings);

    public AID getRuralAid(int i);
    public AID getHelicopterAid(int i);
}
