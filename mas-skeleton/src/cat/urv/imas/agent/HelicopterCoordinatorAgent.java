/**
 * IMAS code for the practical work.
 * Copyright (C) 2014 DEIM - URV
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.behaviour.helicopterCoordinator.SplitMapBehaviour;
import cat.urv.imas.behaviour.helicopterCoordinator.HelicopterCoordinatorWaitForReadyBehaviour;
import cat.urv.imas.onthology.GameMap;
import cat.urv.imas.onthology.MapArea;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.onthology.Person;
import jade.core.*;
import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.FIPANames.InteractionProtocol;
import jade.lang.acl.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Helicopter Coordinator agent is in charge of controlling Helicopter Agents.
 */
public class HelicopterCoordinatorAgent extends ImasAgent {

    GameMap map;
    private Map<MapArea,List<AID>> helicoptersPerArea;
    private List<Person> peopleToSave;
    private Map<MapArea, PriorityQueue<Person>> injuredPerArea;
    public int runningContractNets;
    
    /**
     * Builds the Helicopter coordinator agent.
     */
    public HelicopterCoordinatorAgent() {
        super(AgentType.HELICOPTER_COORDINATOR);
    }

    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {

        /* ** Very Important Line (VIL) ************************************* */
        // Without any explanation of course
        this.setEnabledO2ACommunication(true, 1);
        /* ********************************************************************/
        
        Object[] args = getArguments();
        map = (GameMap)args[0];
        peopleToSave = new ArrayList<>();
        injuredPerArea = new HashMap<>();
        runningContractNets = 0;
        
        // Register the agent to the DF
        ServiceDescription sd1 = new ServiceDescription();
        sd1.setType(AgentType.HELICOPTER_COORDINATOR.toString());
        sd1.setName(getLocalName());
        sd1.setOwnership(OWNER);
        
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.addServices(sd1);
        dfd.setName(getAID());
        try {
            DFService.register(this, dfd);
            log("Registered to the DF");
        } catch (FIPAException e) {
            System.err.println(getLocalName() + " registration with DF failed. Reason: " + e.getMessage());
            doDelete();
        }

        log("created a HelicopterCoordinatorAgent");
        
        addBehaviour(new SplitMapBehaviour());
    }

    public GameMap getMap() {
        return map;
    }

    /**
     * @return the helicoptersPerArea
     */
    public Map<MapArea,List<AID>> getHelicoptersPerArea() {
        return helicoptersPerArea;
    }

    /**
     * @param helicoptersPerArea the helicoptersPerArea to set
     */
    public void setHelicoptersPerArea(Map<MapArea,List<AID>> helicoptersPerArea) {
        this.helicoptersPerArea = helicoptersPerArea;
    }

    /**
     * @return the peopleToSave
     */
    public List<Person> getPeopleToSave() {
        return peopleToSave;
    }

    /**
     * @param peopleToSave the peopleToSave to set
     */
    public void setPeopleToSave(List<Person> peopleToSave) {
        this.peopleToSave = peopleToSave;
    }
    
    public List<AID> getHelicopterAgents(){
        List<AID> helicopters = new ArrayList<>();
        for (MapArea area:helicoptersPerArea.keySet()){
            helicopters.addAll(helicoptersPerArea.get(area));
        }
        return helicopters;
    }

    /**
     * @return the injuredPerArea
     */
    public Map<MapArea, PriorityQueue<Person>> getInjuredPerArea() {
        return injuredPerArea;
    }
    
    public MapArea getAreaByID(String id){
        for (MapArea area:helicoptersPerArea.keySet()){
            if (area.getId().equals(id)){
                return area;
            }
        }
        return null;
    }
    
    
}
