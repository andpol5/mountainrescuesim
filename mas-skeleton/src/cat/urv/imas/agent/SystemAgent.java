/**
 * IMAS base code for the practical work.
 * Copyright (C) 2014 DEIM - URV
 * <p/>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * <p/>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p/>
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.behaviour.system.SystemWaitForReadyBehaviour;
import cat.urv.imas.map.Cell;
import cat.urv.imas.onthology.InitialGameSettings;
import cat.urv.imas.onthology.GameSettings;
import cat.urv.imas.gui.GraphicInterface;
import cat.urv.imas.behaviour.system.RequestResponseBehaviour;
import cat.urv.imas.gui.Statistics;
import jade.core.*;
import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.FIPANames.InteractionProtocol;
import jade.lang.acl.*;
import jade.wrapper.*;

import java.util.Iterator;
import java.util.List;

/**
 * System sagent that controls the GUI and loads initial configuration settings.
 */
public class SystemAgent extends ImasAgent implements IAgentCreator {
    

    /**
     * GUI with the map, system agent log and statistics.
     */
    private GraphicInterface gui;
    /**
     * Game settings. At the very beginning, it will contain the loaded initial
     * configuration settings.
     */
    private GameSettings game;
    /**
     * The Coordinator agent with which interacts sharing game settings every
     * round.
     */
    private AID coordinatorAgent;
    /**
     * Set of statistics to show up.
     */
    private Statistics statistics;

    /**
     * Builds the System agent.
     */
    public SystemAgent() {
        super(AgentType.SYSTEM);
    }

    public static final String RURAL_NAME = "rural";
    public static final String HELICOPTER_NAME = "helicopter";

    /**
     * A message is shown in the log area of the GUI, as well as in the stantard
     * output.
     *
     * @param log String to show
     */
    @Override
    public void log(String log) {
        if (gui != null) {
            gui.log(getLocalName() + ": " + log + "\n");
        }
        super.log(log);
    }

    /**
     * An error message is shown in the log area of the GUI, as well as in the
     * error output.
     *
     * @param error Error to show
     */
    @Override
    public void errorLog(String error) {
        if (gui != null) {
            gui.log("ERROR: " + getLocalName() + ": " + error + "\n");
        }
        super.errorLog(error);
    }

    /**
     * Gets the game settings.
     *
     * @return game settings.
     */
    public GameSettings getGame() {
        return this.game;
    }

    /**
     * Informs about the set of statistics.
     *
     * @return statistics.
     */
    public Statistics getStatistics() {
        return this.statistics;
    }

    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {

        /* ** Very Important Line (VIL) ************************************* */
        this.setEnabledO2ACommunication(true, 1);

        // Register the agent to the DF
        ServiceDescription sd1 = new ServiceDescription();
        sd1.setType(AgentType.SYSTEM.toString());
        sd1.setName(getLocalName());
        sd1.setOwnership(OWNER);

        DFAgentDescription dfd = new DFAgentDescription();
        dfd.addServices(sd1);
        dfd.setName(getAID());
        try {
            DFService.register(this, dfd);
            log("Registered to the DF");
        } catch (FIPAException e) {
            System.err.println(getLocalName() + " failed registration to DF [ko]. Reason: " + e.getMessage());
            doDelete();
        }

        // Load game settings and create rurals and helicopters
        this.game = InitialGameSettings.load("game.settings", this);
        log("Initial configuration settings loaded");
        
        // Create coordinator agents
        // Coordinators will also have the game map
        jade.wrapper.AgentContainer containter = getContainerController();
        Object[] paramsGameMap = new Object[1];
        paramsGameMap[0] = game.getGameMap();
        UtilsAgents.createAgent(containter, "helicopterCoord", "cat.urv.imas.agent.HelicopterCoordinatorAgent", paramsGameMap);
        UtilsAgents.createAgent(containter, "ruralCoord", "cat.urv.imas.agent.RuralCoordinatorAgent", paramsGameMap);

        this.statistics = new Statistics(this.game.getRuralAgentCost(), this.game.getHelicopterCost());


        // Load GUI
        try {
            this.gui = new GraphicInterface(game);
            gui.showStatistics(statistics);
            gui.setVisible(true);
            log("GUI loaded");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // search CoordinatorAgent
        ServiceDescription searchCriterion = new ServiceDescription();
        searchCriterion.setType(AgentType.COORDINATOR.toString());
        this.coordinatorAgent = UtilsAgents.searchAgent(this, searchCriterion);
        // searchAgent is a blocking method, so we will obtain always a correct AID

        // add behaviours
        // we wait for the initialization of the game
        MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchProtocol(InteractionProtocol.FIPA_REQUEST), MessageTemplate.MatchPerformative(ACLMessage.REQUEST));

        addBehaviour(new RequestResponseBehaviour(this, mt));
        addBehaviour(new SystemWaitForReadyBehaviour());

        // Setup finished. When the last inform is received, the agent itself will add
        // a behaviour to send/receive actions
    }

    public void updateGUI() {
        this.gui.showStatistics(statistics);
        this.gui.updateGame();
    }

    @Override
    public void createRuralAgent(Cell cellForRural, int i, GameSettings settings) {
        jade.wrapper.AgentContainer containter = getContainerController();
        Object[] paramsForRural = new Object[2];
        paramsForRural[0] = cellForRural;
        paramsForRural[1] = settings.getGameMap();
        jade.wrapper.AgentController agentController = UtilsAgents.createAgent(containter, "rural" + i, "cat.urv.imas.agent.RuralAgent", paramsForRural);
    }

    @Override
    public void createHelicopterAgent(Cell cellForHelicopter, int i, GameSettings settings) {
        jade.wrapper.AgentContainer containter = getContainerController();
        Object[] paramsForHelicopter = new Object[2];
        paramsForHelicopter[0] = cellForHelicopter;
        paramsForHelicopter[1] = settings.getGameMap();
        jade.wrapper.AgentController agentController = UtilsAgents.createAgent(containter, HELICOPTER_NAME + i, "cat.urv.imas.agent.HelicopterAgent", paramsForHelicopter);
    }

    @Override
    public AID getRuralAid(int i) {
        return new AID(RURAL_NAME+i, AID.ISLOCALNAME);
    }

    @Override
    public AID getHelicopterAid(int i) {
        return new AID(HELICOPTER_NAME+i, AID.ISLOCALNAME);
    }
    
}
